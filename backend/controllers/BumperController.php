<?php

namespace backend\controllers;

use backend\models\search\BumperSearch;
use backend\models\search\InventorySearch;
use common\models\Bumper;
use common\models\BumperComment;
use common\models\Inventory;
use common\models\UploadExcel;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BumperController implements the CRUD actions for Bumper model.
 */
class BumperController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bumper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BumperSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $yearList = Bumper::getYearList();

        $makeList = Bumper::getMakeList();

        $queryParams = Yii::$app->request->queryParams;

        $make = $queryParams['BumperSearch']['MAKE'] ?? null;

        $modelList = Bumper::getModelList($make);

        $partTypeList = Bumper::getPartTypeList();

        $excelUpload = new UploadExcel();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'yearList' => $yearList,
            'makeList' => $makeList,
            'modelList' => $modelList,
            'partTypeList' => $partTypeList,
            'excelUpload' => $excelUpload
        ]);
    }

    /**
     * Displays a single Bumper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $bumperModel = $this->findModel($id);
        $inventoryModel = new Inventory();
        $searchModel = new InventorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $bumperModel->PartsLink);
        $totalInStock = $bumperModel->getTotalInStock();
        $finishedInStock = $bumperModel->getFinishedInStock();

        if ($bumperModel->comment) {
            $bumperCommentModel = BumperComment::findOne($bumperModel->comment->id);
        } else {
            $bumperCommentModel = new BumperComment();
        }

        return $this->render('view', [
            'bumperModel' => $bumperModel,
            'inventoryModel' => $inventoryModel,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalInStock' => $totalInStock,
            'finishedInStock' => $finishedInStock,
            'bumperCommentModel' => $bumperCommentModel,
        ]);
    }

    /**
     * Finds the Bumper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bumper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bumper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Render PDF file into browser window.
     *
     * @param $id
     * @return mixed
     */
    public function actionLoadPdf($id)
    {
        $bumper = $this->findModel($id);
        $totalInStock = $bumper->getTotalInStock();
        $finishedInStock = $bumper->getFinishedInStock();

        $content = $this->renderPartial('_pdf', [
            'bumper' => $bumper,
            'totalInStock' => $totalInStock,
            'finishedInStock' => $finishedInStock,
        ]);

        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        $pdf->filename = $bumper->PartsLink . '.pdf';

        return $pdf->render();
    }
}
