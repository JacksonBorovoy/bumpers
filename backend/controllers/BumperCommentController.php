<?php

namespace backend\controllers;

use common\models\BumperComment;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BumperCommentController implements the CRUD actions for BumperComment model.
 */
class BumperCommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update' => ['post'],
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new BumperComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $bumperId
     * @return mixed
     */
    public function actionCreate($bumperId)
    {
        $model = new BumperComment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['bumper/view', 'id' => $bumperId]);
        } else {
            return $this->redirect(['bumper/view', 'id' => $bumperId]);
        }
    }

    /**
     * Updates an existing BumperComment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param $bumperId
     * @return mixed
     */
    public function actionUpdate($id, $bumperId)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['bumper/view', 'id' => $bumperId]);
        } else {
            return $this->redirect(['bumper/view', 'id' => $bumperId]);
        }
    }

    /**
     * Finds the BumperComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BumperComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BumperComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
