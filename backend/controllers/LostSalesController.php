<?php

namespace backend\controllers;

use backend\models\search\LostSalesSearch;
use common\models\Bumper;
use common\models\LostSales;
use common\models\LostSalesQuantity;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * LostSalesController implements the CRUD actions for LostSales model.
 */
class LostSalesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LostSales models.
     * @return mixed
     */
    public function actionHistory()
    {
        $searchModel = new LostSalesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $partTypeList = Bumper::getPartTypeList();
        $yearList = Bumper::getYearList();
        $makeList = Bumper::getMakeList();

        $queryParams = Yii::$app->request->queryParams;

        $make = $queryParams['LostSalesSearch']['bumper.MAKE'] ?? null;

        $modelList = Bumper::getModelList($make);

        return $this->render('history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'partTypeList' => $partTypeList,
            'yearList' => $yearList,
            'makeList' => $makeList,
            'modelList' => $modelList,
        ]);
    }

    /**
     * Lists summary report.
     * @return string
     */
    public function actionSummary()
    {
        $searchModel = new LostSalesSearch();
        $dataProvider = $searchModel->searchSummary(Yii::$app->request->queryParams);

        return $this->render('summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LostSales model via AJAX call.
     *
     * @return array
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $partsLink = Yii::$app->request->post('partsLink');

        if ($partsLink) {
            $model = new LostSales();
            $model->parts_link = $partsLink;
            $model->created_at = date('Y-m-d H:i:s');

            if ($model->save()) {
                $quantityModel = LostSalesQuantity::findOne(['parts_link' => $model->parts_link]);
                if (!$quantityModel) {
                    $quantityModel = new LostSalesQuantity();
                    $quantityModel->parts_link = $model->parts_link;
                    $quantityModel->save();
                }

                if ($quantityModel->updateCounters(['quantity' => 1])) {
                    return [
                        'code' => 0,
                        'message' => 'OK',
                    ];
                }

                $model->delete();
                return [
                    'code' => 1,
                    'message' => 'An error occurred during incrementing item quantity.',
                ];
            }
            return [
                'code' => 1,
                'message' => 'An error occurred during marking item as missed sale.',
            ];
        }

        return [
            'code' => 1,
            'message' => 'Parts Link is missing in request.',
        ];
    }

    /**
     * Finds the LostSales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LostSales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LostSales::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
