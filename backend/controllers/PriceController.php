<?php

namespace backend\controllers;

use backend\models\PriceForm;
use common\models\KeyStorageItem;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class PriceController
 * @package backend\controllers
 */
class PriceController extends Controller
{
    /**
     * @return string|\yii\web\Response
     */
    public function actionUpdate()
    {
        $priceSuggestedForm = new PriceForm();
        $priceSuggestedForm->setModel($this->findModel('suggestedListPrice'));
        $priceShopForm = new PriceForm();
        $priceShopForm->setModel($this->findModel('bodyShopCost'));
        $dates = [
            'suggestedListPrice' => $priceSuggestedForm,
            'bodyShopCost' => $priceShopForm
        ];
        if (Model::loadMultiple($dates, Yii::$app->request->post()) && Model::validateMultiple($dates)) {
            foreach ($dates as $item) {
                $item->model->value = $item->value;
                $item->model->save();
            }
            Yii::$app->session->setFlash('pricesUpdated', 'Saved!');
            return $this->redirect(['update']);
        }
        return $this->render('update', [
            'model' => $dates
        ]);
    }

    /**
     * @param $key
     * @return KeyStorageItem
     * @throws NotFoundHttpException
     */
    protected function findModel($key)
    {
        if (($model = KeyStorageItem::findOne(['key' => $key])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}