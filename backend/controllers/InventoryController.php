<?php

namespace backend\controllers;

use backend\models\search\InventorySearch;
use common\models\Bumper;
use common\models\Inventory;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * InventoryController implements the CRUD actions for Inventory model.
 */
class InventoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['POST'],
                    'updateFinished' => ['POST'],
                    'sell' => ['POST'],
                    'discard' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Show Sales history
     *
     * @return string
     */
    public function actionSalesHistory()
    {
        $searchModel = new InventorySearch();
        $dataProvider = $searchModel->searchSales(Yii::$app->request->queryParams);

        $makeList = Bumper::getMakeList();

        $queryParams = Yii::$app->request->queryParams;

        $make = $queryParams['InventorySearch']['bumper.MAKE'] ?? null;

        $modelList = Bumper::getModelList($make);

        return $this->render('sales-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'makeList' => $makeList,
            'modelList' => $modelList,
        ]);
    }

    /**
     * Show Discard history
     *
     * @return string
     */
    public function actionDiscardHistory()
    {
        $searchModel = new InventorySearch();
        $dataProvider = $searchModel->searchDiscard(Yii::$app->request->queryParams);

        $makeList = Bumper::getMakeList();

        $queryParams = Yii::$app->request->queryParams;

        $make = $queryParams['InventorySearch']['bumper.MAKE'] ?? null;

        $modelList = Bumper::getModelList($make);

        return $this->render('discard-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'makeList' => $makeList,
            'modelList' => $modelList,
        ]);
    }

    /**
     * Creates new inventory item.
     *
     * @return array|Inventory
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Inventory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        } else {
            $response = [
                'code' => 1,
                'message' => 'An error occurred during adding inventory.'
            ];
            return $response;
        }
    }

    /**
     * Update is_finished value by AJAX call
     *
     * @return array
     */
    public function actionUpdateFinished()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        if ($id) {
            $isFinished = Yii::$app->request->post('isFinished');

            if (isset($isFinished)) {
                $model = $this->findModel($id);
                $model->is_finished = $isFinished == 1 ? 1 : 0;

                if ($model->save()) {
                    return [
                        'code' => 0,
                        'message' => 'OK',
                    ];
                } else {
                    return [
                        'code' => 1,
                        'message' => 'An error occurred during saving inventory.',
                    ];
                }
            }

            return [
                'code' => 1,
                'message' => 'isFinished value is missing in request.',
            ];
        }

        return [
            'code' => 1,
            'message' => 'Inventory ID is missing in request.',
        ];
    }

    /**
     * Finds the Inventory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inventory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inventory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Perform the sell action via AJAX call
     *
     * @return array
     */
    public function actionSell()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        if ($id) {
            $model = $this->findModel($id);
            $model->sold_at = date('Y-m-d H:i:s');

            if ($model->save()) {
                return [
                    'code' => 0,
                    'message' => 'OK',
                ];
            } else {
                return [
                    'code' => 1,
                    'message' => 'An error occurred during selling inventory.',
                ];
            }
        }

        return [
            'code' => 1,
            'message' => 'Inventory ID is missing in request.',
        ];
    }

    /**
     * Perform the discard action via AJAX call
     *
     * @return array
     */
    public function actionDiscard()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        if ($id) {
            $model = $this->findModel($id);
            $model->discarded_at = date('Y-m-d H:i:s');

            if ($model->save()) {
                return [
                    'code' => 0,
                    'message' => 'OK',
                ];
            } else {
                return [
                    'code' => 1,
                    'message' => 'An error occurred during discarding inventory.',
                ];
            }
        }

        return [
            'code' => 1,
            'message' => 'Inventory ID is missing in request.',
        ];
    }
}
