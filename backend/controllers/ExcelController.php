<?php


namespace backend\controllers;

use common\models\Inventory;
use Yii;
use yii\base\UserException;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\UploadExcel;
use yii\web\HttpException;
use yii\web\UploadedFile;

class ExcelController extends Controller
{
    public function actionExport()
    {
        $inventory = (new  yii\db\Query)->createCommand()->setSql("SELECT `PartsLink`, GROUP_CONCAT(DISTINCT `MAKE` SEPARATOR ','), GROUP_CONCAT(DISTINCT `Model` SEPARATOR ','), GROUP_CONCAT(DISTINCT `Year` SEPARATOR ','), count(DISTINCT inventory.id) as `Quantity` 
                                                                    FROM bumpers 
                                                                    INNER JOIN inventory ON inventory.parts_link=bumpers.PartsLink 
                                                                    WHERE `sold_at` IS NULL AND `discarded_at` IS NULL 
                                                                    GROUP BY `PartsLink`")
            ->queryAll();
        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Inventory' => [
                    'data' => $inventory,
                    'titles' => ['Partslink number', 'MAKE', 'Model', 'Year', 'Quantity'],
                    'callbacks' => [
                        // $cell is a PHPExcel_Cell object
                        'A' => function ($cell, $row, $column) {
                            $cell->getStyle()->applyFromArray([
                                'font' => [
                                    'bold' => true,
                                ],
                                'alignment' => [
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                ],
                                'borders' => [
                                    'top' => [
                                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    ],
                                ],
                                'fill' => [
                                    'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
//                                    'rotation' => 90,
                                    'startcolor' => [
                                        'argb' => 'FFA0A0A0',
                                    ],
                                    'endcolor' => [
                                        'argb' => 'FFFFFFFF',
                                    ],
                                ],
                            ]);
                        },
                        'B' => function ($cell, $row, $column) {
                            $cell->getStyle()->applyFromArray([
                                'alignment' => [
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                ],
                            ]);
                        },
                        'C' => function ($cell, $row, $column) {
                            $cell->getStyle()->applyFromArray([
                                'alignment' => [
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                ],
                            ]);
                        },
                        'D' => function ($cell, $row, $column) {
                            $cell->getStyle()->applyFromArray([
                                'alignment' => [
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                ],
                            ]);
                        },
                        'E' => function ($cell, $row, $column) {
                            $cell->getStyle()->applyFromArray([
                                'alignment' => [
                                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                ],
                            ]);
                        }
                    ],
                ]
            ]
        ]);
        $file->getWorkbook()->getSheet()->getColumnDimension('A')->setAutoSize(true);
        $file->getWorkbook()->getSheet()->getColumnDimension('B')->setAutoSize(true);
        $file->getWorkbook()->getSheet()->getColumnDimension('C')->setAutoSize(true);
        $file->getWorkbook()->getSheet()->getColumnDimension('D')->setAutoSize(true);
        $file->getWorkbook()->getSheet()->getColumnDimension('E')->setAutoSize(true);
        $time = date("Y-m-d_H-i-s");
        $file->send("inventory-$time.xlsx");
    }

    public function actionImport()
    {
        $excelModel = new UploadExcel();

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstance($excelModel, 'excelFile');

            try {
                $inputFileType = \PHPExcel_IOFactory::identify($file->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($file->tempName);
            } catch (Exception $e) {
                throwException($e);
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            if ($sheet->getCell('A1')->getValue() === 'PART #') {
                Inventory::deleteAll(['sold_at' => null, 'discarded_at' => null]);
                $dataInsert = [];
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':C' . $row, null, true, false);
                    if ($rowData[0][0] !== null && $rowData[0][1]) {
                        $data = $this->sortData($rowData, $rowData[0][1]);
                        foreach ($data as $item) {
                            $dataInsert[] = $item;
                        }
                    }
                    if ($rowData[0][2] !== null) {
                        $data = $this->sortData($rowData, $rowData[0][2], 1);
                        foreach ($data as $item) {
                            $dataInsert[] = $item;
                        }
                    }
                }
                Yii::$app->getDb()->createCommand()->batchInsert('inventory',
                    ['parts_link', 'grade', 'is_finished', 'created_at'], $dataInsert)->execute();
            } else {
                Yii::$app->session->setFlash('error', "Your value in title is not correct!");
            }
        }
        return $this->redirect(['bumper/index']);
    }

    private function sortData(array $rowData, $count, $finished = 0)
    {
        $timeNow = date("Y-m-d H:i:s");
        $data = [];
        $i = 0;
        do {
            if ($rowData[0][0] != null) {
                $data[] = [
                    $rowData[0][0],
                    'A',
                    $finished,
                    $timeNow
                ];
            }
            $i++;
        } while ($i <= $count - 1);
        return $data;
    }
}