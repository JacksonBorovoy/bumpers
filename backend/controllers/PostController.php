<?php

namespace backend\controllers;

use backend\models\search\PostSearch;
use common\models\Bumper;
use common\models\Post;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post', 'get']
                ]
            ]
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['published_at' => SORT_DESC]
        ];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $yearList = Bumper::getYearList();

            $makeList = Bumper::getMakeList();

            return $this->render('create', [
                'model' => $model,
                'yearList' => $yearList,
                'makeList' => $makeList,
            ]);
        }
    }

    /**
     * Action to GET Model list via AJAX call
     */
    public function actionGetAjaxModelList()
    {
        $allParams = Yii::$app->request->post('depdrop_all_params', null);
        $make = ArrayHelper::getValue($allParams, 'make');
        $selectedModel = ArrayHelper::getValue($allParams, 'model-hidden');
        if (!empty($make)) {
            $list = Bumper::getModelList($make);
            $response = [];
            foreach ($list as $item) {
                $response[] = ['id' => $item, 'name' => $item];
            }

            echo Json::encode(['output' => $response, 'selected' => $selectedModel]);
            return;
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $yearList = Bumper::getYearList();

            $makeList = Bumper::getMakeList();

            return $this->render('update', [
                'model' => $model,
                'yearList' => $yearList,
                'makeList' => $makeList,
            ]);
        }
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
