<?php

namespace backend\controllers;


use common\models\Inventory;
use common\models\LostSales;
use Yii;
use common\models\RequestBumper;
use backend\models\search\RequestBumpersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * RequestBumperController implements the CRUD actions for RequestBumper model.
 */
class RequestBumperController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestBumper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestBumpersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RequestBumper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RequestBumper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequestBumper();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RequestBumper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RequestBumper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestBumper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RequestBumper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RequestBumper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Perform the sell action via AJAX call
     *
     * @return array
     */
    public function actionApprove()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        if ($id) {
            $model = $this->findModel($id);
            $iFinished = Inventory::find()
                ->where(['parts_link' => $model->partsLink])
                ->andWhere(['is', 'sold_at', null])
                ->andWhere(['=', 'is_finished', 1])
                ->one();

            $iNotFinished = Inventory::find()
                ->where(['parts_link' => $model->partsLink])
                ->andWhere(['is', 'sold_at', null])
                ->andWhere(['=', 'is_finished', 0])
                ->one();
            $inventory = (!empty($iFinished)) ? $iFinished : $iNotFinished;

            if ($inventory) {
                $inventory->sold_at = date('Y-m-d H:i:s');
                $model->status = 1;
                if ($inventory->save() && $model->save()) {
                    return [
                        'code' => 0,
                        'message' => 'OK',
                    ];
                } else {
                    return [
                        'code' => 1,
                        'message' => 'An error occurred during selling inventory.',
                    ];
                }
            } else {
                return [
                    'code' => 1,
                    'message' => 'You haven`t got this product',
                ];
            }

        }

        return [
            'code' => 1,
            'message' => 'Inventory ID is missing in request.',
        ];
    }

    public function actionDecline()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        if ($id) {
            $model = $this->findModel($id);
            $lostSalesModel = new LostSales();
            $lostSalesModel->parts_link = $model->partsLink;
            $lostSalesModel->created_at = date('Y-m-d H:i:s');
            $model->status = 0;
            if ($lostSalesModel->save() && $model->save()) {
                return [
                    'code' => 0,
                    'message' => 'OK',
                ];
            } else {
                return [
                    'code' => 1,
                    'message' => 'An error occurred during selling in lost_sales.',
                ];
            }
        }

        return [
            'code' => 1,
            'message' => 'RequestBumper ID is missing in request.',
        ];
    }
}


//$iNotFinished = Inventory::find()
//    ->where(['parts_link' => $model->partsLink])
//    ->andWhere(['is', 'sold_at', null])
//    ->andWhere(['is', 'is_finished', '0'])
//    ->one();
//$inventory = (!empty($iFinished)) ? $iFinished : $iNotFinished;
//var_dump($inventory); die;