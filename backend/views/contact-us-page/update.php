<?php

/* @var $this yii\web\View */
use yii\bootstrap\Alert;

/* @var $model common\models\ContactUsPage */

$this->title = 'Update Contact Us Page';
$this->params['breadcrumbs'][] = ['label' => 'Contact Us Page', 'url' => ['update', 'id' => $model->id]];
?>

<?php if (Yii::$app->session->hasFlash('contactUsUpdated')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => Yii::$app->session->getFlash('contactUsUpdated'),
    ]);
} ?>

<div class="contact-us-page-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
