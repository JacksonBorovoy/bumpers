<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactUsPage */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?php $mapsApiUrl = "https://maps.googleapis.com/maps/api/js?key=" . Yii::$app->params['googleMapsApiKey']; ?>
<?php $this->registerJsFile($mapsApiUrl); ?>
<?php $this->registerJsFile("@web/js/contact-us-page.js", ['depends' => 'yii\web\YiiAsset']); ?>

<div class="contact-us-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'about_us')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 200,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => false,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'map_lat')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'map_lng')->hiddenInput()->label(false) ?>

    <div id="googleMap" style="height: 400px; margin-bottom: 20px;"></div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
