<?php

use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\WarrantyPage */

$this->title = 'Update Warranty Page';
$this->params['breadcrumbs'][] = ['label' => 'Warranty Page', 'url' => ['update', 'id' => $model->id]];
?>

<?php if (Yii::$app->session->hasFlash('warrantyUpdated')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => Yii::$app->session->getFlash('warrantyUpdated'),
    ]);
} ?>

<div class="warranty-page-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
