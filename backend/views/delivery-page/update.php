<?php

use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\DeliveryPage */

$this->title = 'Update Delivery Page';
$this->params['breadcrumbs'][] = ['label' => 'Delivery Page', 'url' => ['update', 'id' => $model->id]];
?>

<?php if (Yii::$app->session->hasFlash('deliveryUpdated')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => Yii::$app->session->getFlash('deliveryUpdated'),
    ]);
} ?>

<div class="delivery-page-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
