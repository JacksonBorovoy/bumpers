<?php

use common\models\LostSales;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LostSalesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $partTypeList */
/* @var array $yearList */
/* @var array $makeList */
/* @var array $modelList */

$this->title = 'Lost Sales';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .btn-default {
        height: 35px;
    }
</style>

<div class="lost-sales">
    <?= Html::a('Summmary Report', ['summary'], [
        'class' => 'btn btn-success',
        'style' => 'float: right',
    ]) ?>

    <?php Pjax::begin(['id' => 'lost-sales-pjax',]); ?>

    <p>
        <?php $gridColumns = [
            [
                'attribute' => 'created_at',
                'format' => 'date',
                'label' => 'Date',
                'filterOptions' => [
                    'style' => 'width: 185px;',
                ],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createTimeRange',
                    'convertFormat' => true,
                    'startAttribute' => 'createTimeStart',
                    'endAttribute' => 'createTimeEnd',
                    'pluginOptions' => [
                        'timePicker' => false,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'Y-m-d'
                        ]
                    ],
                ])
            ],
            'parts_link',
            [
                'attribute' => 'bumper.Year',
                'filter' => $yearList,
            ],
            [
                'attribute' => 'bumper.MAKE',
                'filter' => $makeList
            ],
            [
                'attribute' => 'bumper.Model',
                'filter' => $modelList,
            ],
            [
                'attribute' => 'bumper.FrontRear',
                'filter' => [
                    'Front' => 'Front',
                    'Rear' => 'Rear',
                ],
            ],
            [
                'attribute' => 'bumper.PartType',
                'filter' => $partTypeList,
            ],
            [
                'label' => 'Body Shop Cost',
                'value' => function (LostSales $model) {
                    return Yii::$app->formatter->asCurrency($model->bumper->getBodyShopPrice(), 'USD');
                }
            ],
        ]; ?>

        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_SELF,
            'pjaxContainerId' => 'lost-sales-pjax',
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
            'filename' => 'report-lost-sales-' . date('Y-m-d'),
        ]); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
