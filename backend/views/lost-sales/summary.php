<?php

use common\models\LostSales;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LostSalesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Summary Report';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .btn-default {
        height: 35px;
    }
</style>

<div class="summary-report">
    <?php Pjax::begin(['id' => 'summary-report-pjax',]); ?>

    <p>
        <?php $gridColumns = [
            [
                'attribute' => 'created_at',
                'format' => 'date',
                'label' => 'Date',
                'filterOptions' => [
                    'style' => 'width: 185px;',
                ],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createTimeRange',
                    'convertFormat' => true,
                    'startAttribute' => 'createTimeStart',
                    'endAttribute' => 'createTimeEnd',
                    'pluginOptions' => [
                        'timePicker' => false,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'Y-m-d'
                        ]
                    ],
                ])
            ],
            'parts_link',
            [
                'attribute' => 'quantity.quantity',
                'label' => 'Qty Lost',
                'footer' => 'Total Sales Lost'
            ],
            [
                'label' => 'Sales $ Lost',
                'value' => function (LostSales $model) {
                    $total = $model->quantity->quantity * $model->bumper->getBodyShopPrice();
                    return Yii::$app->formatter->asCurrency($total, 'USD');
                },
                'footer' => Yii::$app->formatter->asCurrency(LostSales::getTotalAmount($dataProvider->getModels()), 'USD'),
            ],
        ]; ?>

        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_SELF,
            'pjaxContainerId' => 'summary-report-pjax',
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
            'filename' => 'report-summary-' . date('Y-m-d'),
        ]); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'showFooter' => true,
    ]); ?>

    <?php Pjax::end(); ?>
</div>
