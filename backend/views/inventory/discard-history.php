<?php

use common\models\Inventory;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\InventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $makeList */
/* @var array $modelList */

$this->title = 'Discard History';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .btn-default {
        height: 35px;
    }
</style>
<div class="discard-history">
    <?php Pjax::begin(['id' => 'discard-history-pjax']); ?>
    <p>
        <?php $gridColumns = [
            [
                'attribute' => 'discarded_at',
                'format' => 'date',
                'label' => 'Date',
                'filterOptions' => [
                    'style' => 'width: 185px;',
                ],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createTimeRange',
                    'convertFormat' => true,
                    'startAttribute' => 'createTimeStart',
                    'endAttribute' => 'createTimeEnd',
                    'pluginOptions' => [
                        'timePicker' => false,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'Y-m-d'
                        ]
                    ],
                ])
            ],
            [
                'attribute' => 'grade',
                'filter' => [
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                ],
            ],
            [
                'attribute' => 'bumper.MAKE',
                'filter' => $makeList,
            ],
            [
                'attribute' => 'bumper.Model',
                'filter' => $modelList,
            ],
            [
                'attribute' => 'bumper.FrontRear',
                'filter' => [
                    'Front' => 'Front',
                    'Rear' => 'Rear',
                ],
            ],
            'bumper.Price:currency',
            [
                'label' => 'List Price',
                'value' => function (Inventory $model) {
                    return Yii::$app->formatter->asCurrency($model->bumper->getSuggestedListPrice(), 'USD');
                }
            ],
            [
                'label' => 'Body Shop Cost',
                'value' => function (Inventory $model) {
                    return Yii::$app->formatter->asCurrency($model->bumper->getBodyShopPrice(), 'USD');
                }
            ],
        ]; ?>

        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_SELF,
            'pjaxContainerId' => 'discard-history-pjax',
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
            'filename' => 'report-discard-' . date('Y-m-d'),
        ]); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
