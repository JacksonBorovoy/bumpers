<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestBumper */

$this->title = 'Update Request Bumper: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Request Bumpers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="request-bumper-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
