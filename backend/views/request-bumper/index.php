<?php

use common\models\RequestBumper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RequestBumpersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile("@web/js/requestBumpers.js", ['depends' => 'yii\web\YiiAsset']);
$this->title = 'Request Bumpers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-bumper-index">

    <?php Pjax::begin(); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table table-responsive grid-view',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'email:email',
            'phone',
            'partsLink',

            [
                'label' => 'Status',
                'value' => function (RequestBumper $model) {
                    $approve = Html::a('Approve', 'javascript:void(0)', [
                        'class' => 'btn bg-olive btn-sm',
                        'data-id' => $model->id,
                        'onclick' => 'approve(this)',
                    ]);
                    $decline = Html::a('Decline', 'javascript:void(0)', [
                        'class' => 'btn bg-navy btn-sm',
                        'data-id' => $model->id,
                        'onclick' => 'decline(this)',
                    ]);
                    $html = "<span class='margin-right-15'>$approve</span><span class='margin-right-15'>$decline</span>";

                    if ($model->status === 1) {
                        return 'Approoved';
                    } elseif ($model->status === 0) {
                        return 'Declined';
                    } else {
                        return $html;
                    }
                },
                'format' => 'raw'
            ],

        ]
    ]); ?>

    <?php Pjax::end(); ?>

</div>
