<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RequestBumper */

$this->title = 'Create Request Bumper';
$this->params['breadcrumbs'][] = ['label' => 'Request Bumpers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-bumper-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
