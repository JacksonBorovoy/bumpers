<?php

use backend\assets\BackendAsset;
use common\models\Bumper;
use common\models\Inventory;
use common\widgets\Lightbox;
use kartik\daterange\DateRangePicker;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $bumperModel common\models\Bumper */
/* @var $bumperCommentModel common\models\BumperComment */
/* @var $inventoryModel common\models\Inventory */
/* @var $searchModel backend\models\search\InventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $totalInStock string */
/* @var $finishedInStock string */

$bundle = BackendAsset::register($this);
$this->registerJsFile("@web/js/inventory.js", ['depends' => 'yii\web\YiiAsset']);

$this->title = "Part details - " . $bumperModel->PartsLink;
$this->params['breadcrumbs'][] = ['label' => 'Inventories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$images = [
    [
        'thumb' => $bumperModel->getImage('ImageIn', $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')),
        'original' => $bumperModel->getImage('ImageIn', $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')),
        'group' => 'test',
        'linkOptions' => ['class' => 'thumbnail'],
        'thumbOptions' => ['style' => 'height: 150px;'],
        'withWrapper' => true,
        'wrapperClass' => 'col-md-4'
    ],
    [
        'thumb' => $bumperModel->getImage('ImageOut', $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')),
        'original' => $bumperModel->getImage('ImageOut', $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')),
        'group' => 'test',
        'linkOptions' => ['class' => 'thumbnail'],
        'thumbOptions' => ['style' => 'height: 150px;'],
        'withWrapper' => true,
        'wrapperClass' => 'col-md-4'
    ],
    [
        'thumb' => $bumperModel->getImage('ImageTop', $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')),
        'original' => $bumperModel->getImage('ImageTop', $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')),
        'group' => 'test',
        'linkOptions' => ['class' => 'thumbnail'],
        'thumbOptions' => ['style' => 'height: 150px;'],
        'withWrapper' => true,
        'wrapperClass' => 'col-md-4'
    ]
];
?>
<div class="bumper-view">
    <div class="margin-bottom">
        <?= Html::a('Manage Inventory', ['create'], [
            'class' => 'btn btn-success popup-modal-manage-inventory',
            'data-toggle' => 'modalManageInventory',
            'data-target' => '#modalManageInventory',
            'data-name' => $bumperModel->PartsLink,
            'id' => 'popupModalManageInventory',
        ]) ?>

        <?= Html::a('Print an "Out"', ['/bumper/load-pdf', 'id' => $bumperModel->bumperid], [
            'class' => 'btn bg-navy',
            'style' => 'float: right',
            'target' => '_blank',
            'data-toggle' => 'tooltip',
            'title' => 'Will open the generated PDF file in a new window'
        ]); ?>

        <?= Html::a('Missed Sale', 'javascript:void(0)', [
            'class' => 'btn bg-purple margin-r-5 popup-modal-missed-sale',
            'data-toggle' => 'modalMissedSale',
            'data-target' => '#modalMissedSale',
            'id' => 'popupModalMissedSale',
            'style' => 'float: right',
        ]); ?>
    </div>
    <h4>Images
        <small>(click on image to enlarge)</small>
    </h4>
    <hr>
    <div class="row">
        <?= Lightbox::widget([
            'files' => $images
        ]) ?>
    </div>
    <h4>Part information</h4>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <?php echo DetailView::widget([
                'model' => $bumperModel,
                'attributes' => [
                    'Year',
                    'MAKE',
                    'Model',
                    'FrontRear',
                    'PartType',
                    'OemNum',
                    'PartsLink',
                    [
                        'label' => 'Stamped Number',
                        'format' => 'html',
                        'value' => function (Bumper $bumperModel) {
                            $stampedNumber = $bumperModel->getStampedNumber();
                            return !empty($stampedNumber) ? $stampedNumber : null;
                        },
                    ],
                    [
                        'label' => 'Plastic',
                        'value' => function (Bumper $bumperModel) {
                            $plastic = $bumperModel->getPlastic();
                            return !empty($plastic) ? $plastic : null;
                        },
                    ],
                    [
                        'label' => 'Comments',
                        'value' => function (Bumper $bumperModel) {
                            $comments = $bumperModel->getComments();
                            return !empty($comments) ? $comments : null;
                        },
                    ],
                    [
                        'label' => 'Cores in stock:',
                        'value' => $totalInStock,
                    ],
                    [
                        'label' => 'Finished in stock:',
                        'value' => $finishedInStock,
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#usa_price" data-toggle="tab">USA Price</a></li>
                    <li><a href="#canada_price" data-toggle="tab">Canada Price</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="usa_price">
                        <div class="row">
                            <div class="col-6 blockOptionsRowTitle">
                                OEM Price
                            </div>
                            <div class="col text-right">
                                <?= Yii::$app->formatter->asCurrency($bumperModel->Price, 'USD') ?>
                            </div>
                        </div>
                        <div class="row active">
                            <div class="col-6 blockOptionsRowTitle">
                                Suggested List Price
                            </div>
                            <div class="col text-right">
                                <?= Yii::$app->formatter->asCurrency($bumperModel->getSuggestedListPrice(),
                                    'USD') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 blockOptionsRowTitle">
                                Body Shop Cost
                            </div>
                            <div class="col text-right">
                                <?= Yii::$app->formatter->asCurrency($bumperModel->getBodyShopPrice(), 'USD') ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="canada_price">
                        <div class="row">
                            <div class="col-6 blockOptionsRowTitle">
                                OEM Price
                            </div>
                            <div class="col text-right">
                                <?= Yii::$app->formatter->asCurrency($bumperModel->COEPrice, 'CAD') ?>
                            </div>
                        </div>
                        <div class="row active">
                            <div class="col-6 blockOptionsRowTitle">
                                Suggested List Price
                            </div>
                            <div class="col text-right">
                                <?= Yii::$app->formatter->asCurrency($bumperModel->getSuggestedListPrice('canada'),
                                    'CAD') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 blockOptionsRowTitle">
                                Body Shop Cost
                            </div>
                            <div class="col text-right">
                                <?= Yii::$app->formatter->asCurrency($bumperModel->getBodyShopPrice('canada'),
                                    'CAD') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php $action = $bumperCommentModel->isNewRecord ? Url::toRoute([
                        'bumper-comment/create',
                        'bumperId' => $bumperModel->bumperid
                    ]) : Url::toRoute([
                        'bumper-comment/update',
                        'id' => $bumperCommentModel->id,
                        'bumperId' => $bumperModel->bumperid
                    ]); ?>
                    <?php $form = ActiveForm::begin(['action' => $action]); ?>

                    <?php $bumperCommentModel->parts_link = $bumperModel->PartsLink; ?>
                    <?= $form->field($bumperCommentModel, 'parts_link')->hiddenInput()->label(false) ?>

                    <?= $form->field($bumperCommentModel, 'text')->textarea(['rows' => 6]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'header' => '<h2 class="modal-title"></h2>',
    'id' => 'modal-manage-inventory',
]); ?>

<div class="inventory-form margin-bottom">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'action' => ['inventory/create'],
        'id' => 'form-inventory-create',
    ]); ?>

    <?= $form->field($inventoryModel, 'parts_link', [
        'options' => ['tag' => false],
        'template' => '{input}'
    ])->hiddenInput(['value' => $bumperModel->PartsLink])->label(false) ?>

    <div class="row">
        <div class="col-md-3 margin-bottom">
            <?= $form->field($inventoryModel, 'grade',
                ['template' => "{input}", 'options' => ['tag' => false]])->dropDownList([
                'A' => 'A',
                'B' => 'B',
                'C' => 'C',
                'D' => 'D',
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Add Item', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php Pjax::begin(['enablePushState' => false]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        [
            'attribute' => 'created_at',
            'format' => 'date',
            'filterOptions' => [
                'style' => 'width: 185px;',
            ],
            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'createTimeRange',
                'convertFormat' => true,
                'startAttribute' => 'createTimeStart',
                'endAttribute' => 'createTimeEnd',
                'pluginOptions' => [
                    'timePicker' => false,
                    'timePickerIncrement' => 30,
                    'locale' => [
                        'format' => 'Y-m-d'
                    ]
                ],
            ])
        ],
        [
            'attribute' => 'grade',
            'filter' => [
                'A' => 'A',
                'B' => 'B',
                'C' => 'C',
                'D' => 'D',
            ],
        ],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'multiple' => false,
            'checkboxOptions' => function (Inventory $model, $key, $index, $column) {
                return [
                    'value' => $model->is_finished ? 0 : 1,
                    'checked' => $model->is_finished,
                    'data-id' => $model->id,
                    'onchange' => 'updateFinished(this)',
                ];
            },
            'header' => 'Finished',
            'name' => 'finishedSelection'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'options' => [
                'style' => 'width:150px;'
            ],
            'template' => '<span class="margin-right-15">{sell}</span><span class="margin-right-15">{discard}</span>',
            'buttons' => [
                'sell' => function ($url, $model, $key) {
                    return Html::a('Sell', 'javascript:void(0)', [
                        'class' => 'btn bg-olive btn-sm',
                        'data-id' => $model->id,
                        'onclick' => 'sellInventory(this)',
                    ]);
                },
                'discard' => function ($url, $model, $key) {
                    return Html::a('Discard', 'javascript:void(0)', [
                        'class' => 'btn bg-navy btn-sm',
                        'data-id' => $model->id,
                        'onclick' => 'discardInventory(this)',
                    ]);
                },
            ],
        ],
    ],
]); ?>

<?php Pjax::end(); ?>

<?php Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h2 class="modal-title"></h2>',
    'id' => 'modal-missed-sale',
    'footer' => Html::a('OK', '', [
        'class' => 'btn btn-danger',
        'id' => 'missed-sale-confirm',
        'data-partslink' => $bumperModel->PartsLink,
    ]),
]); ?>
<?= 'Are you sure you want to lost sale this item ?' ?>
<?php Modal::end(); ?>
