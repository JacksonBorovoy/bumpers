<?php

use backend\assets\BackendAsset;

/* @var $bumper common\models\Bumper */
/* @var $totalInStock string */
/* @var $finishedInStock string */

$bundle = BackendAsset::register($this);
?>

<div class="row" style="margin-bottom: 30px;">
    <div class="col-md-12">
        <h4 class="text-center">
            <?= 'User Company' ?> - <?= 'User address' ?> - <?= 'User City' ?>, <?= 'User Province' ?> <?= 'User ZIP' ?>
        </h4>
        <h4 class="text-center">Phone: <?= 'User Phone' ?></h4>
        <h4 class="text-center">Fax: <?= 'User Fax' ?></h4>
        <h3 class="text-center" style="margin-top: 45px;">We are <span class="text-primary">OUT OF STOCK</span> on this
            item.</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-6">
        <h2><?= $bumper->Year . ' ' . $bumper->Model ?>
            <small><?= strtoupper($bumper->FrontRear) ?> BUMPER COVER</small>
        </h2>
        <table class="table" style="margin-top: 20px;">
            <tbody>
            <tr>
                <td>Year(s)</td>
                <td><?= $bumper->Year ?></td>
            </tr>
            <tr>
                <td>Make</td>
                <td><?= $bumper->MAKE ?></td>
            </tr>
            <tr>
                <td>Model</td>
                <td><?= $bumper->Model ?></td>
            </tr>
            <tr>
                <td>Front/Real</td>
                <td><?= $bumper->FrontRear ?></td>
            </tr>
            <tr>
                <td>OEM Number</td>
                <td><?= $bumper->OemLong ?></td>
            </tr>
            <tr>
                <td>Unworked Cores in Stock</td>
                <td><?= $totalInStock ?></td>
            </tr>
            <tr>
                <td>Finished Cores in Stock</td>
                <td><?= $finishedInStock ?></td>
            </tr>
            <tr>
                <td>Comments</td>
                <td><?= htmlspecialchars($bumper->comment['text']) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ccol-md-6 col-lg-6 col-sm-6">
        <h3>Pricing</h3>
        <table class="table">
            <tbody>
            <tr>
                <td></td>
                <td>USA</td>
                <td>Canada</td>
            </tr>
            <tr>
                <td>OEM Price</td>
                <td><?= Yii::$app->formatter->asCurrency($bumper->Price, 'USD') ?></td>
                <td><?= Yii::$app->formatter->asCurrency($bumper->COEPrice, 'CAD') ?></td>
            </tr>
            <tr>
                <td>Suggested List Price</td>
                <td><?= Yii::$app->formatter->asCurrency($bumper->getSuggestedListPrice(),
                        'USD') ?></td>
                <td><?= Yii::$app->formatter->asCurrency($bumper->getSuggestedListPrice('canada'),
                        'CAD') ?></td>
            </tr>
            <tr>
                <td><b>Body Shop Cost</b></td>
                <td><b><?= Yii::$app->formatter->asCurrency($bumper->getBodyShopPrice(), 'USD') ?></b></td>
                <td><b><?= Yii::$app->formatter->asCurrency($bumper->getBodyShopPrice('canada'),
                            'CAD') ?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<img src="<?= $bumper->getImage('ImageIn',
    $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')) ?>"
     style="height: 100px; width: 200px; display: inline-block; margin-right: 35px;">

<img src="<?= $bumper->getImage('ImageOut',
    $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')) ?>"
     style="height: 100px; width: 200px; display: inline-block; margin-right: 35px;">

<img src="<?= $bumper->getImage('ImageTop',
    $this->assetManager->getAssetUrl($bundle, 'img/no-image.png')) ?>"
     style="height: 100px; width: 200px; display: inline-block;">

<h3 class="text-center" style="margin-top: 20px">
    USE OUR PART NUMBER WHEN YOU ORDER:
    <span class="text-danger"><?= $bumper['PartsLink'] ?></span>
</h3>
