<?php

use common\models\Bumper;
use common\models\UploadExcel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BumperSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $yearList */
/* @var array $makeList */
/* @var array $modelList */
/* @var array $partTypeList */
/* @var UploadExcel $excelUpload */

$this->title = Yii::t('backend', 'Bumpers inventory');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .field-uploadexcel-excelfile input[type='file'] {
        display: none;
    }

</style>
<div class="upload-file">
    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-error alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4>Please, check the file</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['excel/import'],
    ]); ?>

    <div class="form-group">
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="file_browser"
                        onclick="$('#uploadexcel-excelfile').trigger('click')">
                <i class="fa fa-search"></i> Browse</button>
            </span>
        </div>
        <span id="nameFile"></span>
        <div><?= $form->field($excelUpload, 'excelFile')->fileInput()->label(false); ?></div>
        <?php echo Html::submitButton('Upload', ['class' => 'btn btn-success upload']) ?>
        <?php echo Html::a('Download', ['excel/export'], ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="inventory-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'MAKE',
                'filter' => $makeList,
            ],
            [
                'attribute' => 'Model',
                'filter' => $modelList,
            ],
            [
                'attribute' => 'Year',
                'filter' => $yearList,
            ],
            [
                'attribute' => 'FrontRear',
                'filter' => [
                    'Front' => 'Front',
                    'Rear' => 'Rear',
                ],
            ],
            [
                'attribute' => 'PartType',
                'filter' => $partTypeList,
            ],
            'Price:currency',
            'COEPrice:currency',
            [
                'label' => 'List Price',
                'value' => function (Bumper $model) {
                    return Yii::$app->formatter->asCurrency($model->getSuggestedListPrice(), 'USD');
                }
            ],
            [
                'label' => 'Body Shop Cost',
                'value' => function (Bumper $model) {
                    return Yii::$app->formatter->asCurrency($model->getBodyShopPrice(), 'USD');
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'header' => 'Actions',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('', ['view', 'id' => $model->bumperid], [
                            'class' => 'btn btn-default btn-xs glyphicon glyphicon-eye-open',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        $('#uploadexcel-excelfile').change(function () {
            var name = $(this).val().substr($(this).val().lastIndexOf('\\') + 1);
            $('#nameFile').html(name);
            var extension = name.split('.');
            if(extension[1] == 'xlsx' || extension[1] == 'xls') {
                $(".upload").prop("disabled", false);
            }
        });

        $(".upload").prop("disabled", true);
   });
</script>
