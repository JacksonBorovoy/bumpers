<?php
/* @var $this yii\web\View */
/* @var $model common\models\PostCategory */
/* @var $categories common\models\PostCategory[] */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Post Category',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-category-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'categories' => $categories
    ]) ?>

</div>
