<?php

use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\HomePage */

$this->title = 'Update Home Page';
$this->params['breadcrumbs'][] = ['label' => 'Home Page', 'url' => ['update', 'id' => $model->id]];
?>

<?php if (Yii::$app->session->hasFlash('homePageUpdated')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => Yii::$app->session->getFlash('homePageUpdated'),
    ]);
} ?>

<div class="home-page-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
