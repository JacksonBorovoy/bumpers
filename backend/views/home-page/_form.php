<?php

use trntv\filekit\widget\Upload;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\HomePage */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?php $this->registerJsFile("@web/js/home-page.js", ['depends' => 'yii\web\YiiAsset']); ?>

<div class="home-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'video_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_text')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 200,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => false,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ) ?>

    <?php echo $form->field($model, 'video_link')->textInput(['maxlength' => true]) ?>

    <div class="row margin-bottom">
        <div class="col-md-6">
            <div id="youtubePlayer" class="youtubePlayer"></div>
        </div>
    </div>

    <?= $form->field($model, 'videoBackground')->widget(
        Upload::className(),
        [
            'url' => ['/file-storage/upload'],
            'maxFileSize' => 50000000, // 50 MiB
            'acceptFileTypes' => new JsExpression('/(\.|\/)(mp4)$/i'),
        ]);
    ?>

    <?php echo $form->field($model, 'services_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'services_text')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 200,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => false,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ) ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Comparison slider</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'imgBefore')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
                        ]);
                    ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'imgAfter')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton('Save', ['class' => 'btn btn-primary', 'id' => 'submitButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
