<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel \backend\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Pricing');
?>

<?php if (Yii::$app->session->hasFlash('pricesUpdated')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => Yii::$app->session->getFlash('pricesUpdated'),
    ]);
} ?>

<div class="price-index">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-8">
            <h3>Definitions:</h3>
            <p>
                "Suggested List" and "Body Shop Cost"
                Suggested List" is the price that the body shop would charge the customer or insurance company. "Body
                Shop Cost" is the price that you will charge the body shop.
            </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2">
            <h4>Suggested List Price:</h4>
        </div>
        <div class="col-md-3">
            <?php $suggestedListPriceHint = 'If you want the Suggested List Price to be 25% lower than the OEM price, enter "25".'; ?>
            <?= $form->field($model['suggestedListPrice'],
                '[suggestedListPrice]value')->label(false)->hint($suggestedListPriceHint); ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2">
            <h4>Body Shop Cost:</h4>
        </div>
        <div class="col-md-3">
            <?php $bodyShopCostHint = 'If you want the Body Shop Cost to be 25% lower than the Suggested List Price, enter "25".'; ?>
            <?= $form->field($model['bodyShopCost'], '[bodyShopCost]value')->label(false)->hint($bodyShopCostHint); ?>
        </div>
    </div>
    <?php echo Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>
</div>