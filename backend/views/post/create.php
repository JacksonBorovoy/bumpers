<?php
/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var array $yearList */
/* @var array $makeList */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Post',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'yearList' => $yearList,
        'makeList' => $makeList,
    ]) ?>

</div>
