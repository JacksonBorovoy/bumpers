<?php

use kartik\depdrop\DepDrop;
use trntv\filekit\widget\Upload;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\bootstrap\ActiveForm */
/* @var array $yearList */
/* @var array $makeList */
?>

<?php $this->registerJsFile("@web/js/post.js", ['depends' => 'yii\web\YiiAsset']); ?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you\'ll leave this field empty, URL name will be generated automatically'))
        ->textInput(['maxlength' => true]) ?>

    <?php $model->category_id = 1; ?>
    <?= $form->field($model, 'category_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'body')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 200,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => false,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    )->label('Content') ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'year')->dropDownList($yearList, ['prompt' => 'Select year...']); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'make')->dropDownList($makeList, ['id' => 'make', 'prompt' => 'Select make...']) ?>
        </div>
        <div class="col-md-4">
            <?= Html::hiddenInput('model-hidden', $model->model, ['id' => 'model-hidden']) ?>
            <?= $form->field($model, 'model')->widget(DepDrop::className(), [
                'options' => ['id' => 'model'],
                'pluginOptions' => [
                    'depends' => ['make'],
                    'placeholder' => 'Select model...',
                    'url' => Url::to(['post/get-ajax-model-list']),
                    'params' => ['model-hidden'],
                    'initialize' => $model->isNewRecord ? false : true,
                ],
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'tagValues')->label('Tags')->textInput()->hint('Comma separated values'); ?>

    <?= $form->field($model, 'thumbnail')->widget(
        Upload::className(),
        [
            'url' => ['/file-storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB
        ]);
    ?>

    <?= $form->field($model, 'attachments')->widget(
        Upload::className(),
        [
            'url' => ['/file-storage/upload'],
            'sortable' => true,
            'maxFileSize' => 10000000, // 10 MiB
            'maxNumberOfFiles' => 10,
        ])->label('Gallery images');
    ?>

    <?= $form->field($model, 'video_url'); ?>

    <div class="row margin-bottom">
        <div class="col-md-6">
            <div id="youtubePlayer" class="youtubePlayer"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Comparison slider</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'imgBefore')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                        ]);
                    ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'imgAfter')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'submitButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
