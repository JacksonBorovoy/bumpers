<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <p>
        <?= Html::a(
            Yii::t('backend', 'Create {modelClass}', ['modelClass' => 'Post']),
            ['create'],
            ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => [

            'id',
            'title',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'options' => [
                    'style' => 'width:145px;'
                ],
                'template' => '<span class="margin-right-15">{update}</span><span class="margin-right-15">{delete}</span><span>{link}</span>',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-warning btn-xs glyphicon glyphicon-pencil',
                        ]);
                    },
                    'link' => function ($url, $model, $key) {
                        return Html::a('', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-xs glyphicon glyphicon-link',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger btn-xs glyphicon glyphicon-trash popup-modal-delete',
                            'data-toggle' => 'modalDelete',
                            'data-target' => '#modalDelete',
                            'data-id' => $model->id,
                            'data-name' => $model->title,
                            'id' => 'popupModalDelete',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>

<?php Modal::begin([
    'header' => '<h2 class="modal-title"></h2>',
    'id' => 'modal-delete',
    'footer' => Html::a('Delete', '', ['class' => 'btn btn-danger', 'id' => 'delete-confirm']),
]); ?>

<?= 'Are you sure you want to delete this item ?'; ?>

<?php Modal::end(); ?>

