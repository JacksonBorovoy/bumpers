<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var array $yearList */
/* @var array $makeList */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => 'Post',
    ]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="article-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'yearList' => $yearList,
        'makeList' => $makeList,
    ]) ?>

</div>
