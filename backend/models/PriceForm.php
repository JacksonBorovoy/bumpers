<?php

namespace backend\models;

use common\models\KeyStorageItem;
use Yii;
use yii\base\Model;

class PriceForm extends Model
{
    public $key;
    public $value;
    public $comment;

    private $model;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['value'], 'integer', 'min' => 0, 'max' => 100],

        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('common', 'Key'),
            'value' => Yii::t('common', 'Value'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }

    /**
     * @return KeyStorageItem
     */
    public function getModel()
    {
        if (!$this->model) {
            $this->model = new KeyStorageItem();
        }
        return $this->model;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function setModel($model)
    {
        $this->key = $model->key;
        $this->value = $model->value;
        $this->comment = $model->comment;
        $this->model = $model;
        return $this->model;
    }

}
