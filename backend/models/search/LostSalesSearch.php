<?php

namespace backend\models\search;

use common\models\LostSales;
use kartik\daterange\DateRangeBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LostSalesSearch represents the model behind the search form about `common\models\LostSales`.
 */
class LostSalesSearch extends LostSales
{
    public $createTimeRange;

    public $createTimeStart;

    public $createTimeEnd;

    public function __construct(array $config = [])
    {
        $this->createTimeStart = date("Y-m-d", strtotime("last Monday"));
        $this->createTimeEnd = date("Y-m-d", strtotime("Sunday"));
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [
                [
                    'parts_link',
                    'created_at',
                    'bumper.Year',
                    'bumper.MAKE',
                    'bumper.Model',
                    'bumper.FrontRear',
                    'bumper.PartType',
                ], 'safe'
            ],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'bumper.MAKE',
            'bumper.Model',
            'bumper.FrontRear',
            'bumper.Year',
            'bumper.PartType',
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LostSales::find()->groupBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith('bumper as bumper');

        $dataProvider->sort->attributes['bumper.Year'] = [
            'asc' => ['bumper.Year' => SORT_ASC],
            'desc' => ['bumper.Year' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.MAKE'] = [
            'asc' => ['bumper.MAKE' => SORT_ASC],
            'desc' => ['bumper.MAKE' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.Model'] = [
            'asc' => ['bumper.Model' => SORT_ASC],
            'desc' => ['bumper.Model' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.FrontRear'] = [
            'asc' => ['bumper.FrontRear' => SORT_ASC],
            'desc' => ['bumper.FrontRear' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.PartType'] = [
            'asc' => ['bumper.PartType' => SORT_ASC],
            'desc' => ['bumper.PartType' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (is_integer($this->createTimeStart) && is_integer($this->createTimeEnd)) {
            $this->createTimeStart = date('Y-m-d H:i:s', $this->createTimeStart);
            $this->createTimeEnd = date('Y-m-d 23:59:59', $this->createTimeEnd);
        }

        $query->andFilterWhere([
            'bumper.FrontRear' => $this->getAttribute('bumper.FrontRear'),
            'bumper.PartType' => $this->getAttribute('bumper.PartType'),
        ]);

        $query->andFilterWhere(['like', 'parts_link', $this->parts_link]);
        $query->andFilterWhere(['like', 'bumper.Year', $this->getAttribute('bumper.Year')]);
        $query->andFilterWhere(['like', 'bumper.MAKE', $this->getAttribute('bumper.MAKE')]);
        $query->andFilterWhere(['like', 'bumper.Model', $this->getAttribute('bumper.Model')]);
        $query->andFilterWhere(['>=', 'created_at', $this->createTimeStart]);
        $query->andFilterWhere(['<=', 'created_at', $this->createTimeEnd]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchSummary($params)
    {
        $query = LostSales::find()->groupBy('parts_link');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (is_integer($this->createTimeStart) && is_integer($this->createTimeEnd)) {
            $this->createTimeStart = date('Y-m-d', $this->createTimeStart);
            $this->createTimeEnd = date('Y-m-d', $this->createTimeEnd);
        }

        $query->andFilterWhere([
        ]);

        $query->andFilterWhere(['like', 'parts_link', $this->parts_link]);
        $query->andFilterWhere(['>=', 'created_at', $this->createTimeStart]);
        $query->andFilterWhere(['<=', 'created_at', $this->createTimeEnd]);

        return $dataProvider;
    }
}
