<?php

namespace backend\models\search;

use common\models\Inventory;
use kartik\daterange\DateRangeBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * InventorySearch represents the model behind the search form about `common\models\Inventory`.
 */
class InventorySearch extends Inventory
{
    public $createTimeRange;

    public $createTimeStart;

    public $createTimeEnd;

    /**
     * InventorySearch constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->createTimeStart = date("Y-m-d", strtotime("last Monday"));
        $this->createTimeEnd = date("Y-m-d", strtotime("Sunday"));
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'is_finished'], 'integer'],
            [
                [
                    'parts_link',
                    'grade',
                    'created_at',
                    'sold_at',
                    'discarded_at',
                    'dateFrom',
                    'dateTo',
                    'bumper.MAKE',
                    'bumper.Model',
                    'bumper.FrontRear',
                    'bumper.Price'
                ], 'safe'
            ],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/']
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['bumper.MAKE', 'bumper.Model', 'bumper.FrontRear', 'bumper.Price',]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $partsLink
     *
     * @return ActiveDataProvider
     */
    public function search($params, $partsLink)
    {
        // add conditions that should always apply here
        $query = Inventory::find()
            ->where(['parts_link' => $partsLink])
            ->andWhere(['is', 'sold_at', null])
            ->andWhere(['is', 'discarded_at', null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if (is_integer($this->createTimeStart) && is_integer($this->createTimeEnd)) {
            $this->createTimeStart = date('Y-m-d', $this->createTimeStart);
            $this->createTimeEnd = date('Y-m-d', $this->createTimeEnd);
        }

        $query->andFilterWhere(['like', 'grade', $this->grade]);
        $query->andFilterWhere(['>=', 'created_at', $this->createTimeStart]);
        $query->andFilterWhere(['<=', 'created_at', $this->createTimeEnd]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchSales($params)
    {
        // add conditions that should always apply here
        $query = Inventory::find()
            ->andWhere(['not', ['sold_at' => null]])
            ->andWhere(['is', 'discarded_at', null])
            ->groupBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith('bumper as bumper');

        $dataProvider->sort->attributes['bumper.MAKE'] = [
            'asc' => ['bumper.MAKE' => SORT_ASC],
            'desc' => ['bumper.MAKE' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.Model'] = [
            'asc' => ['bumper.Model' => SORT_ASC],
            'desc' => ['bumper.Model' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.FrontRear'] = [
            'asc' => ['bumper.FrontRear' => SORT_ASC],
            'desc' => ['bumper.FrontRear' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.Price'] = [
            'asc' => ['bumper.Price' => SORT_ASC],
            'desc' => ['bumper.Price' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (is_integer($this->createTimeStart) && is_integer($this->createTimeEnd)) {
            $this->createTimeStart = date('Y-m-d H:i:s', $this->createTimeStart);
            $this->createTimeEnd = date('Y-m-d 23:59:59', $this->createTimeEnd);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bumper.Price' => $this->getAttribute('bumper.Price'),
            'grade' => $this->grade,
            'bumper.FrontRear' => $this->getAttribute('bumper.FrontRear'),
        ]);

        $query->andFilterWhere(['like', 'bumper.MAKE', $this->getAttribute('bumper.MAKE')]);
        $query->andFilterWhere(['like', 'bumper.Model', $this->getAttribute('bumper.Model')]);
        $query->andFilterWhere(['>=', 'sold_at', $this->createTimeStart]);
        $query->andFilterWhere(['<=', 'sold_at', $this->createTimeEnd]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchDiscard($params)
    {
        // add conditions that should always apply here
        $query = Inventory::find()
            ->andWhere(['not', ['discarded_at' => null]])
            ->andWhere(['is', 'sold_at', null])
            ->groupBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith('bumper as bumper');

        $dataProvider->sort->attributes['bumper.MAKE'] = [
            'asc' => ['bumper.MAKE' => SORT_ASC],
            'desc' => ['bumper.MAKE' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.Model'] = [
            'asc' => ['bumper.Model' => SORT_ASC],
            'desc' => ['bumper.Model' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.FrontRear'] = [
            'asc' => ['bumper.FrontRear' => SORT_ASC],
            'desc' => ['bumper.FrontRear' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bumper.Price'] = [
            'asc' => ['bumper.Price' => SORT_ASC],
            'desc' => ['bumper.Price' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (is_integer($this->createTimeStart) && is_integer($this->createTimeEnd)) {
            $this->createTimeStart = date('Y-m-d H:i:s', $this->createTimeStart);
            $this->createTimeEnd = date('Y-m-d 23:59:59', $this->createTimeEnd);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bumper.Price' => $this->getAttribute('bumper.Price'),
            'grade' => $this->grade,
            'bumper.FrontRear' => $this->getAttribute('bumper.FrontRear'),
        ]);

        $query->andFilterWhere(['like', 'bumper.MAKE', $this->getAttribute('bumper.MAKE')]);
        $query->andFilterWhere(['like', 'bumper.Model', $this->getAttribute('bumper.Model')]);
        $query->andFilterWhere(['>=', 'discarded_at', $this->createTimeStart]);
        $query->andFilterWhere(['<=', 'discarded_at', $this->createTimeEnd]);

        return $dataProvider;
    }
}
