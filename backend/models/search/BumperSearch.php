<?php

namespace backend\models\search;

use common\models\Bumper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BumperSearch represents the model behind the search form about `common\models\Bumper`.
 */
class BumperSearch extends Bumper
{
    /**
     * @var string $stampedNumber
     */
    public $stampedNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MAKE', 'Model', 'Year', 'FrontRear', 'PartType', 'OemNum', 'PartsLink', 'stampedNumber'], 'safe'],
            [['Price', 'COEPrice',], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bumper::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Price' => $this->Price,
            'COEPrice' => $this->COEPrice,
            'PartType' => $this->PartType,
        ]);

        $query->andFilterWhere(['like', 'MAKE', $this->MAKE])
            ->andFilterWhere(['like', 'Model', $this->Model])
            ->andFilterCompare('Y1', $this->Year, '<=')
            ->andFilterCompare('Y2', $this->Year, '>=')
            ->andFilterWhere(['like', 'FrontRear', $this->FrontRear])
            ->andFilterWhere(['like', 'OemNum', $this->OemNum])
            ->andFilterWhere(['like', 'PartsLink', $this->PartsLink]);

        if (!empty($this->stampedNumber)) {
            $query->leftJoin('qryUSCtable', 'bumpers.PartsLink = qryUSCtable.PartsLink')
                ->andFilterWhere(['like', 'qryUSCtable.StampedNumber', $this->stampedNumber]);
        }

        return $dataProvider;
    }
}
