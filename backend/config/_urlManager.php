<?php
return [
    'class' => yii\web\UrlManager::class,
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        [
            'pattern' => 'pages/home-page/<id:\d+>',
            'route' => 'home-page/update',
            'defaults' => ['id' => 1],
        ],
        [
            'pattern' => 'pages/contact-us-page/<id:\d+>',
            'route' => 'contact-us-page/update',
            'defaults' => ['id' => 1],
        ],
        [
            'pattern' => 'pages/delivery-page/<id:\d+>',
            'route' => 'delivery-page/update',
            'defaults' => ['id' => 1],
        ],
        [
            'pattern' => 'pages/warranty-page/<id:\d+>',
            'route' => 'warranty-page/update',
            'defaults' => ['id' => 1],
        ],
    ],
];
