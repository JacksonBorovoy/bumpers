function initialize() {
    var lat = document.getElementById('contactuspage-map_lat').value;
    var lng = document.getElementById('contactuspage-map_lng').value;

    var myLatlng = new google.maps.LatLng(lat, lng);

    var mapProp = {
        center: myLatlng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP

    };

    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        draggable: true
    });

    // marker drag event
    google.maps.event.addListener(marker, 'drag', function (event) {
        document.getElementById('contactuspage-map_lat').value = event.latLng.lat();
        document.getElementById('contactuspage-map_lng').value = event.latLng.lng();
    });

    //marker drag event end
    google.maps.event.addListener(marker, 'dragend', function (event) {
        document.getElementById('contactuspage-map_lat').value = event.latLng.lat();
        document.getElementById('contactuspage-map_lng').value = event.latLng.lng();
    });
}

google.maps.event.addDomListener(window, 'load', initialize);