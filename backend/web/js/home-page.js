$(document).ready(function () {
    function getYoutubeVideoId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length === 11) {
            return match[2];
        } else {
            return false;
        }
    }

    function renderYoutubeIframe() {
        var inputValue = $('#homepage-video_link').val();
        var id = getYoutubeVideoId(inputValue);
        var player = $('#youtubePlayer');

        if (id) {
            var iframe = '<iframe src="//www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>';
            player.html(iframe);
            player.css({"padding-bottom": "56.25%", "padding-top": "30px"});
        } else {
            player.html('');
            player.css({"padding-bottom": "0", "padding-top": "0"});
        }

        if (inputValue !== '' && player.html() === '') {
            $('#submitButton').prop('disabled', true);
        } else {
            $('#submitButton').prop('disabled', false);
        }
    }

    $('#homepage-video_link').on('change', function () {
        renderYoutubeIframe();
    });

    renderYoutubeIframe();
});