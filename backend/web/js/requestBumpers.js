
function approve(element) {
    var message = "Are you sure you want to approve this request?";
    if (confirm(message)) {
        var item = $(element);
        $.ajax({
            url: '/request-bumper/approve',
            type: 'POST',
            data: {
                id: item.data('id')
            },
            success: function (response) {
                if (response.code) {
                    alert(response.message);
                } else {
                    $('.grid-view').yiiGridView('applyFilter');
                }
            },
            error: function () {
                alert('Internal server error. Please contact support');
            }
        });
    }
}

function decline(element) {
    var message = "Are you sure you want to decline this request?";
    if (confirm(message)) {
        var item = $(element);
        $.ajax({
            url: '/request-bumper/decline',
            type: 'POST',
            data: {
                id: item.data('id')
            },
            success: function (response) {
                if (response.code) {
                    alert(response.message);
                } else {
                    // Perform PJAX
                    $('.grid-view').yiiGridView('applyFilter');
                }
            },
            error: function () {
                alert('Internal server error. Please contact support');
            }
        });
    }
}

