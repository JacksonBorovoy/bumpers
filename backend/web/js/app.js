$(function () {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();

    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

    // MODALS
    // Modal delete
    $('.popup-modal-delete').click(function (e) {
        e.preventDefault();
        var modal = $('#modal-delete').modal('show');
        modal.find('.modal-body').load($('.modal-dialog'));
        var that = $(this);
        var id = that.data('id');
        var name = that.data('name');
        modal.find('.modal-title').text('Delete the item "' + name + '"');

        $('#delete-confirm').click(function (e) {
            e.preventDefault();
            window.location = 'delete?id=' + id;
        });
    });

    // Modal manage inventory
    $('.popup-modal-manage-inventory').click(function (e) {
        e.preventDefault();
        var modal = $('#modal-manage-inventory').modal('show');
        modal.find('.modal-body').load($('.modal-dialog'));
        var that = $(this);
        var name = that.data('name');
        modal.find('.modal-title').text('Inventory of ' + name);
    });

    // Modal lost sale
    $('.popup-modal-missed-sale').click(function (e) {
        e.preventDefault();
        var modal = $('#modal-missed-sale').modal('show');
        modal.find('.modal-body').load($('.modal-dialog'));
        modal.find('.modal-title').text('Missed Sale');

        $('#missed-sale-confirm').click(function (e) {
            e.preventDefault();
            var partsLink = $(this).data('partslink');
            $.ajax({
                url: '/lost-sales/create',
                type: 'POST',
                data: {
                    partsLink: partsLink
                },
                success: function (response) {
                    if (response.code) {
                        modal.find('.modal-body').text(response.message);
                    } else {
                        modal.find('.modal-body').text('Saved!');
                    }
                },
                error: function () {
                    modal.find('.modal-body').text('Internal server error. Please contact support');
                }
            });
        });
    });
    // MODALS - End
});