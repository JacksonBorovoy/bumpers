$(document).ready(function () {
    // Add new item to inventory and perform PJAX
    $('#form-inventory-create').on('beforeSubmit', function (event) {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (response) {
                if (response.code) {
                    alert(response.message);
                } else {
                    // Perform PJAX
                    $('.grid-view').yiiGridView('applyFilter');
                }
            },
            error: function () {
                alert('Internal server error. Please contact support');
            }
        });
    }).on('submit', function (e) {
        e.preventDefault();
    });

});

// Update is_finished value
function updateFinished(element) {
    var item = $(element);
    $.ajax({
        url: '/inventory/update-finished',
        type: 'POST',
        data: {
            id: item.data('id'),
            isFinished: item.val()
        },
        success: function (response) {
            if (response.code) {
                alert(response.message);
            } else {
                // Perform PJAX
                $('.grid-view').yiiGridView('applyFilter');
            }
        },
        error: function () {
            alert('Internal server error. Please contact support');
        }
    });
}

// Sell inventory item
function sellInventory(element) {
    var message = "Are you sure you want to sell this item?";
    if (confirm(message)) {
        var item = $(element);
        $.ajax({
            url: '/inventory/sell',
            type: 'POST',
            data: {
                id: item.data('id')
            },
            success: function (response) {
                if (response.code) {
                    alert(response.message);
                } else {
                    // Perform PJAX
                    $('.grid-view').yiiGridView('applyFilter');
                }
            },
            error: function () {
                alert('Internal server error. Please contact support');
            }
        });
    }
}

// Discard inventory item
function discardInventory(element) {
    var message = "Are you sure you want to discard this item?";
    if (confirm(message)) {
        var item = $(element);
        $.ajax({
            url: '/inventory/discard',
            type: 'POST',
            data: {
                id: item.data('id')
            },
            success: function (response) {
                if (response.code) {
                    alert(response.message);
                } else {
                    // Perform PJAX
                    $('.grid-view').yiiGridView('applyFilter');
                }
            },
            error: function () {
                alert('Internal server error. Please contact support');
            }
        });
    }
}