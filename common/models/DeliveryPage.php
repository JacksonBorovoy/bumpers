<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "delivery_page".
 *
 * @property integer $id
 * @property string $text
 */
class DeliveryPage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['text'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Content',
        ];
    }
}
