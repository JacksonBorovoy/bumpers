<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "home_page".
 *
 * @property integer $id
 * @property string $videobackground_base_url
 * @property string $videobackground_path
 * @property string $video_title
 * @property string $video_text
 * @property string $video_link
 * @property string $imgbefore_base_url
 * @property string $imgbefore_path
 * @property string $imgafter_base_url
 * @property string $imgafter_path
 * @property string $services_title
 * @property string $services_text
 */
class HomePage extends ActiveRecord
{
    /**
     * @var array
     */
    public $videoBackground;

    /**
     * @var array
     */
    public $imgBefore;

    /**
     * @var array
     */
    public $imgAfter;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'home_page';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'videoBackground',
                'pathAttribute' => 'videobackground_path',
                'baseUrlAttribute' => 'videobackground_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'imgBefore',
                'pathAttribute' => 'imgbefore_path',
                'baseUrlAttribute' => 'imgbefore_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'imgAfter',
                'pathAttribute' => 'imgafter_path',
                'baseUrlAttribute' => 'imgafter_base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_text', 'services_text'], 'string'],
            [['videobackground_base_url', 'videobackground_path', 'video_link', 'imgbefore_base_url', 'imgbefore_path', 'imgafter_base_url', 'imgafter_path'], 'string', 'max' => 1024],
            [['video_title', 'services_title'], 'string', 'max' => 255],
            [['videoBackground', 'imgBefore', 'imgAfter'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_title' => 'Video Title',
            'video_text' => 'Video Text',
            'video_link' => 'Video Link',
            'services_title' => 'Services Title',
            'services_text' => 'Services Text',
            'videoBackground' => 'Background Video',
            'imgBefore' => 'Before',
            'imgAfter' => 'After',
        ];
    }
}
