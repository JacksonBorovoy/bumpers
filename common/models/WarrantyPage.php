<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "warranty_page".
 *
 * @property integer $id
 * @property string $text
 */
class WarrantyPage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'warranty_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['text'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Content',
        ];
    }
}
