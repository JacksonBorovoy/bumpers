<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "bumper_comment".
 *
 * @property integer $id
 * @property string $parts_link
 * @property string $text
 */
class BumperComment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bumper_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parts_link'], 'required'],
            [['text'], 'string'],
            [['parts_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parts_link' => 'Parts Link',
            'text' => 'User Note',
        ];
    }
}
