<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact_us_page".
 *
 * @property integer $id
 * @property string $about_us
 * @property string $address
 * @property string $phone_number
 * @property string $fax_number
 * @property string $email
 * @property string $map_lat
 * @property string $map_lng
 */
class ContactUsPage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_us_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_us'], 'string'],
            [['address', 'phone_number', 'fax_number', 'email', 'map_lat', 'map_lng'], 'string', 'max' => 255],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'about_us' => 'About Us',
            'address' => 'Address',
            'phone_number' => 'Phone Number',
            'fax_number' => 'Fax Number',
            'email' => 'Email',
            'map_lat' => 'Map Latitude',
            'map_lng' => 'Map Longitude',
        ];
    }
}
