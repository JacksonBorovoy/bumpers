<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "lost_sales".
 *
 * @property integer $id
 * @property string $parts_link
 * @property string $created_at
 * @property Bumper $bumper
 * @property LostSalesQuantity $quantity
 */
class LostSales extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lost_sales';
    }

    /**
     * @param LostSales[] $provider
     * @return int
     */
    public static function getTotalAmount($provider)
    {
        $total = 0;
        foreach ($provider as $item) {
            $total += ($item->bumper->getBodyShopPrice() * $item->quantity->quantity);
        }

        return $total;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parts_link'], 'required'],
            [['created_at'], 'safe'],
            [['parts_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parts_link' => 'Parts Link',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBumper()
    {
        return $this->hasOne(Bumper::className(), ['PartsLink' => 'parts_link']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuantity()
    {
        return $this->hasOne(LostSalesQuantity::className(), ['parts_link' => 'parts_link']);
    }
}
