<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "qryUSCtable".
 *
 * @property integer $uscid
 * @property string $PartsLink
 * @property string $Plastic
 * @property string $StampedNumber
 * @property string $Comments
 * @property string $ImageIn
 * @property string $ImageOut
 * @property string $ImageTop
 */
class BumperDetails extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qryUSCtable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Comments'], 'string'],
            [['PartsLink', 'Plastic', 'StampedNumber', 'ImageIn', 'ImageOut', 'ImageTop'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uscid' => 'Uscid',
            'PartsLink' => 'Parts Link',
            'Plastic' => 'Plastic',
            'StampedNumber' => 'Stamped Number',
            'Comments' => 'Comments',
            'ImageIn' => 'Image In',
            'ImageOut' => 'Image Out',
            'ImageTop' => 'Image Top',
        ];
    }
}
