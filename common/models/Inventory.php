<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "inventory".
 *
 * @property integer $id
 * @property string $parts_link
 * @property string $grade
 * @property integer $is_finished
 * @property string $created_at
 * @property string $sold_at
 * @property string $discarded_at
 * @property Bumper $bumper
 */
class Inventory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parts_link'], 'required'],
            [['is_finished'], 'integer'],
            [['created_at', 'sold_at', 'discarded_at'], 'safe'],
            [['parts_link'], 'string', 'max' => 255],
            [['grade'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parts_link' => 'Parts Link',
            'grade' => 'Grade',
            'is_finished' => 'Finished',
            'created_at' => 'Date',
            'sold_at' => 'Sold At',
            'discarded_at' => 'Discarded At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBumper()
    {
        return $this->hasOne(Bumper::className(), ['PartsLink' => 'parts_link']);
    }
}
