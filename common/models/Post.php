<?php

namespace common\models;

use common\models\query\PostQuery;
use creocoder\taggable\TaggableBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property string $body
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property string $imgbefore_base_url
 * @property string $imgbefore_path
 * @property string $imgafter_base_url
 * @property string $imgafter_path
 * @property array $attachments
 * @property integer $category_id
 * @property integer $year
 * @property string $make
 * @property string $model
 * @property integer $status
 * @property integer $published_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $tagValues
 * @property string $video_url
 *
 * @property User $author
 * @property User $updater
 * @property PostCategory $category
 * @property PostAttachment[] $postAttachments
 */
class Post extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    /**
     * @var array
     */
    public $attachments;

    /**
     * @var array
     */
    public $thumbnail;

    /**
     * @var array
     */
    public $imgBefore;

    /**
     * @var array
     */
    public $imgAfter;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @return PostQuery
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'postAttachments',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'imgBefore',
                'pathAttribute' => 'imgbefore_path',
                'baseUrlAttribute' => 'imgbefore_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'imgAfter',
                'pathAttribute' => 'imgafter_path',
                'baseUrlAttribute' => 'imgafter_base_url'
            ],
            'taggable' => [
                'class' => TaggableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'category_id'], 'required'],
            [['slug'], 'unique'],
            [['body', 'make', 'model'], 'string'],
            [
                ['published_at'],
                'default',
                'value' => function () {
                    return date(DATE_ISO8601);
                }
            ],
            [['published_at'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            [['category_id'], 'exist', 'targetClass' => PostCategory::className(), 'targetAttribute' => 'id'],
            [['status', 'year'], 'integer'],
            [
                [
                    'slug',
                    'thumbnail_base_url',
                    'thumbnail_path',
                    'imgbefore_base_url',
                    'imgbefore_path',
                    'imgafter_base_url',
                    'imgafter_path',
                    'video_url'
                ],
                'string',
                'max' => 1024
            ],
            [['title'], 'string', 'max' => 512],
            [['make', 'model'], 'string', 'max' => 255],
            [['attachments', 'thumbnail', 'tagValues', 'imgBefore', 'imgAfter'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'URL Name'),
            'title' => Yii::t('common', 'Title'),
            'body' => Yii::t('common', 'Body'),
            'thumbnail' => Yii::t('common', 'Thumbnail'),
            'imgBefore' => Yii::t('common', 'Before'),
            'imgAfter' => Yii::t('common', 'After'),
            'category_id' => Yii::t('common', 'Category'),
            'year' => Yii::t('common', 'Year'),
            'make' => Yii::t('common', 'Make'),
            'model' => Yii::t('common', 'Model'),
            'status' => Yii::t('common', 'Published'),
            'published_at' => Yii::t('common', 'Published At'),
            'created_by' => Yii::t('common', 'Author'),
            'updated_by' => Yii::t('common', 'Updater'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'video_url' => Yii::t('common', 'YouTube link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PostCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostAttachments()
    {
        return $this->hasMany(PostAttachment::className(), ['post_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('{{%post_tag_assn}}', ['post_id' => 'id']);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
}
