<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "bumpers".
 *
 * @property integer $bumperid
 * @property string $MAKE
 * @property string $Model
 * @property integer $Y1
 * @property integer $Y2
 * @property string $VARIABLES
 * @property integer $ID
 * @property string $Year
 * @property string $FrontRear
 * @property string $PartType
 * @property string $ImageIn_tmp
 * @property string $ImageOut_tmp
 * @property string $ImageTop_tmp
 * @property string $OemNum
 * @property string $PartsLink
 * @property resource $Superseded
 * @property string $SupersedeNum
 * @property string $OemLong
 * @property double $Price
 * @property integer $Discontinued
 * @property integer $Aftermarket
 * @property string $ProductionNum_old
 * @property string $Comments_old
 * @property string $Plastic_old
 * @property string $Description
 * @property resource $Updated
 * @property string $UpdateDate
 * @property double $COEPrice
 * @property array $details
 * @property array $inventory
 * @property array $comment
 */
class Bumper extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bumpers';
    }

    /**
     * @return array
     */
    public static function getYearList()
    {
        return self::find()
            ->select('Y1')
            ->distinct()
            ->orderBy('Y1')
            ->indexBy('Y1')
            ->column();
    }

    /**
     * @return array
     */
    public static function getMakeList()
    {
        return self::find()
            ->select('MAKE')
            ->distinct()
            ->orderBy('MAKE')
            ->indexBy('MAKE')
            ->column();
    }

    /**
     * @param $make
     * @return array
     */
    public static function getModelList($make)
    {
        if (!empty($make)) {
            return self::find()
                ->select('Model')
                ->distinct()
                ->where(['MAKE' => $make])
                ->indexBy('Model')
                ->column();
        }
        return [];
    }

    /**
     * @return array
     */
    public static function getPartTypeList()
    {
        return self::find()
            ->select('PartType')
            ->distinct()
            ->orderBy('PartType')
            ->indexBy('PartType')
            ->column();
    }

    /**
     * @return int|string
     */
    public static function getTotalNumber()
    {
        return self::find()->count();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bumperid', 'ID'], 'required'],
            [['bumperid', 'Y1', 'Y2', 'ID', 'Discontinued', 'Aftermarket'], 'integer'],
            [['Superseded', 'Updated'], 'string'],
            [['Price', 'COEPrice'], 'number'],
            [['UpdateDate',], 'safe'],
            [['MAKE', 'Year'], 'string', 'max' => 20],
            [['Model', 'VARIABLES'], 'string', 'max' => 200],
            [['FrontRear'], 'string', 'max' => 10],
            [['PartType'], 'string', 'max' => 40],
            [
                [
                    'ImageIn_tmp',
                    'ImageOut_tmp',
                    'ImageTop_tmp',
                    'OemNum',
                    'PartsLink',
                    'ProductionNum_old',
                    'Comments_old',
                    'Plastic_old',
                    'Description'
                ],
                'string',
                'max' => 255
            ],
            [['SupersedeNum', 'OemLong'], 'string', 'max' => 50],
            [['ID'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bumperid' => 'Bumperid',
            'MAKE' => 'Make',
            'Model' => 'Model',
            'Y1' => 'Y1',
            'Y2' => 'Y2',
            'VARIABLES' => 'Variables',
            'ID' => 'ID',
            'Year' => 'Year',
            'FrontRear' => 'Front or Rear',
            'PartType' => 'Part Type',
            'ImageIn_tmp' => 'Image In Tmp',
            'ImageOut_tmp' => 'Image Out Tmp',
            'ImageTop_tmp' => 'Image Top Tmp',
            'OemNum' => 'OEM number',
            'PartsLink' => 'Partslink number',
            'Superseded' => 'Superseded',
            'SupersedeNum' => 'Supersede Num',
            'OemLong' => 'Oem Long',
            'Price' => 'OEM Price',
            'Discontinued' => 'Discontinued',
            'Aftermarket' => 'Aftermarket',
            'ProductionNum_old' => 'Production Num Old',
            'Comments_old' => 'Comments Old',
            'Plastic_old' => 'Plastic Old',
            'Description' => 'Description',
            'Updated' => 'Updated',
            'UpdateDate' => 'Update Date',
            'COEPrice' => 'CAN Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasMany(BumperDetails::className(), ['PartsLink' => 'PartsLink']);
    }

    /**
     * @return string
     */
    public function getStampedNumber()
    {
        $stampedNumbers = [];
        foreach ($this->details as $detail) {
            $stampedNumbers[] = $detail->StampedNumber;
        }
        return implode('<br>', $stampedNumbers);
    }

    /**
     * @return mixed
     */
    public function getPlastic()
    {
        return ArrayHelper::getValue($this->details, '0.Plastic');
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return ArrayHelper::getValue($this->details, '0.Comments');
    }

    /**
     * @param string $imageType
     * @param null $default
     * @return bool|null|string
     */
    public function getImage(string $imageType, $default = null)
    {
        if ($this->isImageTypeAllowed($imageType)) {
            $imageName = ArrayHelper::getValue($this->details, '0.' . $imageType);

            if ($imageUrl = $this->isImageExists($imageName)) {
                return $imageUrl;
            }
        }
        return $default;
    }

    /**
     * @param string $imageType
     * @return bool
     */
    public function isImageTypeAllowed(string $imageType)
    {
        $imageTypes = ['ImageIn', 'ImageOut', 'ImageTop'];

        return in_array($imageType, $imageTypes) ? $imageType : false;
    }

    /**
     * @param $imageName
     * @return bool|string
     */
    public function isImageExists($imageName)
    {
        if (!empty($imageName)) {
            $url = $this->getImagesUrl() . $imageName;
            if (UrlExists($url)) {
                return $url;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getImagesUrl()
    {
        return Yii::$app->params['dbumperBaseUrl'] . '/images/';
    }

    /**
     * Get number of items in inventory that IS NOT finished
     *
     * @return int|string
     */
    public function getTotalInStock()
    {
        return $this->getInventory()
            ->where(['is_finished' => 0])
            ->andWhere(['is', 'sold_at', null])
            ->andWhere(['is', 'discarded_at', null])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasMany(Inventory::className(), ['parts_link' => 'PartsLink']);
    }

    /**
     * Get number of items in inventory that IS finished
     *
     * @return int|string
     */
    public function getFinishedInStock()
    {
        return $this->getInventory()
            ->where(['is_finished' => 1])
            ->andWhere(['is', 'sold_at', null])
            ->andWhere(['is', 'discarded_at', null])
            ->count();
    }

    /**
     * Get Body Shop Price calculated
     *
     * @param string $type
     * @return float
     */
    public function getBodyShopPrice(string $type = 'usa')
    {
        $suggestedListPrice = $this->getSuggestedListPrice($type);

        $discount = (int)Yii::$app->keyStorage->get('bodyShopCost', 0);

        $bodyShopPrice = $suggestedListPrice - $suggestedListPrice * $discount / 100;

        return $bodyShopPrice;
    }

    /**
     * Get Suggested List Price calculated
     *
     * @param string $type
     * @return float
     */
    public function getSuggestedListPrice(string $type = 'usa')
    {
        $regularPrice = $type === 'canada' ? $this->COEPrice : $this->Price;

        $discount = (int)Yii::$app->keyStorage->get('suggestedListPrice', 0);

        $suggestedListPrice = $regularPrice - $regularPrice * $discount / 100;

        return $suggestedListPrice;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(BumperComment::className(), ['parts_link' => 'PartsLink']);
    }
}
