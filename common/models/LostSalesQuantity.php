<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "lost_sales_quantity".
 *
 * @property string $parts_link
 * @property integer $quantity
 */
class LostSalesQuantity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lost_sales_quantity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parts_link'], 'required'],
            [['quantity'], 'integer'],
            [['parts_link'], 'string', 'max' => 255],
            [['parts_link'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parts_link' => 'Parts Link',
            'quantity' => 'Quantity',
        ];
    }
}
