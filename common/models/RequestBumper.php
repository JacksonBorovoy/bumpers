<?php

namespace common\models;

use Yii;
use yii\validators\ExistValidator;

/**
 * This is the model class for table "request_bumper".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $partsLink
 * @property integer $status
 */
class RequestBumper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_bumper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'phone'], 'required'],
            ['email', 'email'],
            [['phone'], 'match', 'pattern' => '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/'],
            ['status', 'in', 'range' => [0, 1]],
            ['status', 'default', 'value' => null],
            [['name'], 'string', 'max' => 32],
            ['name', 'required', 'message' => 'Please add your name.'],
            [['email', 'phone', 'partsLink'], 'string', 'max' => 255],
            [['partsLink'], 'exist', 'targetClass' => Bumper::className(), 'targetAttribute' => 'PartsLink']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'partsLink' => 'Parts Link',
            'status' => 'Status',
        ];
    }
}
