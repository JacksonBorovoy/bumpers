<?php

namespace common\models\query;

use common\models\PostCategory;
use yii\db\ActiveQuery;

class PostCategoryQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => PostCategory::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * @return $this
     */
    public function noParents()
    {
        $this->andWhere('{{%post_category}}.parent_id IS NULL');

        return $this;
    }
}
