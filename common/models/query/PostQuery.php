<?php

namespace common\models\query;

use common\models\Post;
use creocoder\taggable\TaggableQueryBehavior;
use yii\db\ActiveQuery;

class PostQuery extends ActiveQuery
{
    public function behaviors()
    {
        return [
            TaggableQueryBehavior::className(),
        ];
    }

    public function published()
    {
        $this->andWhere(['status' => Post::STATUS_PUBLISHED]);
        $this->andWhere(['<', '{{%post}}.published_at', time()]);
        return $this;
    }
}
