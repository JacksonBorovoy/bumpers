<?php

use yii\db\Migration;

class m170613_061739_lost_sales_quantity extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('lost_sales_quantity', [
            'parts_link' => $this->string()->notNull()->unique(),
            'quantity' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ]);

        $this->createIndex('partsLink', 'lost_sales_quantity', 'parts_link');
    }

    public function down()
    {
        $this->dropTable('lost_sales_quantity');
    }
}
