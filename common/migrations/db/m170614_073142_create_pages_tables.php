<?php

use yii\db\Migration;

class m170614_073142_create_pages_tables extends Migration
{
    public function up()
    {
        $this->createTable('contact_us_page', [
            'id' => $this->primaryKey(),
            'about_us' => $this->text(),
            'address' => $this->string(),
            'phone_number' => $this->string(),
            'fax_number' => $this->string(),
            'email' => $this->string(),
            'map_lat' => $this->string(),
            'map_lng' => $this->string(),
        ]);

        $this->createTable('home_page', [
            'id' => $this->primaryKey(),
            'videobackground_base_url' => $this->string(1024),
            'videobackground_path' => $this->string(1024),
            'video_title' => $this->string(),
            'video_text' => $this->text(),
            'video_link' => $this->string(1024),
            'imgbefore_base_url' => $this->string(1024),
            'imgbefore_path' => $this->string(1024),
            'imgafter_base_url' => $this->string(1024),
            'imgafter_path' => $this->string(1024),
            'services_title' => $this->string(),
            'services_text' => $this->text(),
        ]);

        $this->createTable('delivery_page', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
        ]);

        $this->createTable('warranty_page', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('contact_us_page');
        $this->dropTable('home_page');
        $this->dropTable('delivery_page');
        $this->dropTable('warranty_page');
    }
}
