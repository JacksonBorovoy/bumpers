<?php

use yii\db\Migration;

class m170614_111521_remove_unused_tables extends Migration
{
    public function up()
    {
        $this->dropTable('page_meta');
        $this->dropTable('page');
    }

    public function down()
    {
        echo "m170614_111521_remove_unused_tables cannot be reverted.\n";

        return false;
    }
}
