<?php

use yii\db\Migration;

class m170601_123653_set_data_to_key_storage_item extends Migration
{
    public function up()
    {
        $this->batchInsert('key_storage_item',
            [
                'key',
                'value',
                'comment',
                'created_at',
                'updated_at'
            ],
            [
                [
                    'suggestedListPrice',
                    10,
                    null,
                    time(),
                    time()
                ],
                [
                    'bodyShopCost',
                    20,
                    null,
                    time(),
                    time()
                ]
        ]);
    }

    public function down()
    {
        $this->delete('key_storage_item', ['key' => 'suggestedListPrice', 'bodyShopCost']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
