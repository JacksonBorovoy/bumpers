<?php

use yii\db\Migration;

class m170522_141126_add_post_comparison_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%post}}', 'imgbefore_base_url', $this->string(1024));
        $this->addColumn('{{%post}}', 'imgbefore_path', $this->string(1024));
        $this->addColumn('{{%post}}', 'imgafter_base_url', $this->string(1024));
        $this->addColumn('{{%post}}', 'imgafter_path', $this->string(1024));
    }

    public function down()
    {
        $this->dropColumn('{{%post}}', 'imgbefore_base_url');
        $this->dropColumn('{{%post}}', 'imgbefore_path');
        $this->dropColumn('{{%post}}', 'imgafter_base_url');
        $this->dropColumn('{{%post}}', 'imgafter_path');
    }
}
