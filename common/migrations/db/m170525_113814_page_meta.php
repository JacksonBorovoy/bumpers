<?php

use yii\db\Migration;

class m170525_113814_page_meta extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page_meta}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'meta_key' => $this->string(255),
            'meta_value' => $this->string(255)

        ], $tableOptions);

        $this->addForeignKey('fk_page', '{{%page_meta}}', 'page_id', '{{%page}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropTable('{{%page_meta}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
