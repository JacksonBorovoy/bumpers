<?php

use yii\db\Migration;

class m170524_133644_seed_default_category extends Migration
{
    public function up()
    {
        $this->insert('post_category', [
            'id' => 1,
            'slug' => 'bumpers',
            'title' => 'Bumpers',
            'body' => null,
            'parent_id' => null,
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('post_category', ['id' => 1]);
    }
}
