<?php

use yii\db\Migration;

class m170614_113523_seed_pages_data extends Migration
{
    public function up()
    {
        $this->insert('home_page', [
            'id' => 1,
            'video_title' => 'Encore Bumpers',
            'video_text' => '<p>Encore Bumpers serves collision centers and body shops surrounding our Rainsville, AL location by providing the highest quality reconditioned OEM plastic bumper covers. Our goal is to provide the highest quality product and best service.</p>',
            'video_link' => 'https://www.youtube.com/watch?v=oUI7bDWVCAE',
            'services_title' => 'Services',
            'services_text' => '<p>Encore Bumpers reconditions OEM bumper covers. With skilled technicians using tried and tested procedures, the end result is a quality bumper cover that is ready to be painted. We use the highest quality repair materials from Polyvance. Our experienced professionals can quickly identify and price bumper covers using dBumper bumper identification service.</p>',
        ]);

        $this->insert('contact_us_page', [
            'id' => 1,
            'about_us' => '<p>Encore Bumpers serves collision centers and body shops surrounding our Rainsville, AL location by providing the highest quality reconditioned OEM plastic bumper covers. Our goal is to provide the highest quality product and best service.</p>',
            'address' => '1128 Kirk Rd SW, Rainsville, AL 35986, USA',
            'phone_number' => '256-638-79912',
            'fax_number' => '256-638-8490',
            'email' => 'info@encorebumpers.com',
            'map_lat' => '34.505526',
            'map_lng' => '-85.897159',
        ]);

        $this->insert('delivery_page', [
            'id' => 1,
            'text' => '<p>DELIVERY</p><p>Encore Bumpers offers free delivery in the Northeast Alabama area. We can deliver your bumper cover to you the next day.</p><p>IMPORTANT NOTE</p><p>We do NOT ship bumper covers. We only deliver in Northeast Alabama.</p>',
        ]);

        $this->insert('warranty_page', [
            'id' => 1,
            'text' => '<p>WARRANTY</p><p>Your Encore Bumpers reconditioned OEM bumper cover is protected by a Limited Lifetime Warranty. Encore Bumpers warrants its bumper covers to be free from defects in material and workmanship for as long as the original purchaser owns the vehicle on which the bumper cover is installed. If failure should occur simply provide proof of purchase to Encore Bumpers, and we will repair or replace the bumper cover at no additional charge. If the repair or replacement bumper cover cannot be provided within two business days from the date of receiving the claim, Encore Bumpers will refund the full original purchase price of the defective bumper cover.</p><p>WARRANTY DOES NOT COVER</p><p>Impact Damages</p><p>Road Hazards</p><p>Improper Installation</p><p>Improper Painting</p><p>DAMAGED / WRONG PARTS</p><p>The customer is responsible for inspecting the reconditioned bumper upon delivery. Any bumper not deemed acceptable should be refused. Care must be taken in the storage of bumpers at the shop to avoid damage.</p>',
        ]);
    }

    public function down()
    {
        echo "m170614_113523_seed_pages_data cannot be reverted.\n";

        return false;
    }
}
