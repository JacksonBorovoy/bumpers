<?php

use yii\db\Migration;

class m170613_131040_create_request_bumper extends Migration
{
    public function up()
    {
        $this->createTable('request_bumper', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'partsLink' => $this->string(255)->defaultValue(null),
            'status' => $this->boolean()->defaultValue(NULL),
        ]);

        $this->createIndex('partsLink', 'request_bumper', 'partsLink');
    }

    public function down()
    {
        $this->dropTable('request_bumper');
    }

}
