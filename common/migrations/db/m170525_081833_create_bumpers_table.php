<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bumpers`.
 */
class m170525_081833_create_bumpers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bumpers', [
            'bumperid' => $this->integer(11)->notNull(),
            'MAKE' => $this->string(20)->defaultValue(null),
            'Model' => $this->string(200)->defaultValue(null),
            'Y1' => $this->smallInteger(4)->unsigned()->defaultValue(null),
            'Y2' => $this->smallInteger(4)->unsigned()->defaultValue(null),
            'VARIABLES' => $this->string(200)->defaultValue(null),
            'ID' => $this->integer(10)->unsigned()->notNull(),
            'Year' => $this->string(20)->defaultValue(null),
            'FrontRear' => $this->string(10)->defaultValue(null),
            'PartType' => $this->string(40)->defaultValue(null),
            'ImageIn_tmp' => $this->string(255)->defaultValue(null),
            'ImageOut_tmp' => $this->string(255)->defaultValue(null),
            'ImageTop_tmp' => $this->string(255)->defaultValue(null),
            'OemNum' => $this->string(255)->defaultValue(null),
            'PartsLink' => $this->string(255)->defaultValue(null),
            'Superseded' => $this->binary(1)->defaultValue(null),
            'SupersedeNum' => $this->string(50)->defaultValue(null),
            'OemLong' => $this->string(50)->defaultValue(null),
            'Price' => $this->double()->defaultValue(null),
            'Discontinued' => $this->smallInteger(1)->defaultValue(null),
            'Aftermarket' => $this->smallInteger(1)->defaultValue(null),
            'ProductionNum_old' => $this->string(255)->defaultValue(null),
            'Comments_old' => $this->string(255)->defaultValue(null),
            'Plastic_old' => $this->string(255)->defaultValue(null),
            'Description' => $this->string(255)->defaultValue(null),
            'Updated' => $this->binary(1)->defaultValue(null),
            'UpdateDate' => $this->dateTime(1)->defaultValue(null),
            'COEPrice' => $this->double(9, 2)->defaultValue(null),
        ]);

        $sql = "ALTER TABLE `bumpers`
                ADD PRIMARY KEY (`bumperid`),
                ADD UNIQUE KEY `iID` (`ID`),
                ADD KEY `iMake` (`MAKE`),
                ADD KEY `iPlink` (`PartsLink`);
                ALTER TABLE `bumpers` ADD FULLTEXT KEY `MAKE` (`MAKE`);";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bumpers');
    }
}
