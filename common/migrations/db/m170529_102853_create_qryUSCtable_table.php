<?php

use yii\db\Migration;

/**
 * Handles the creation of table `qryUSCtable`.
 */
class m170529_102853_create_qryUSCtable_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('qryUSCtable', [
            'uscid' => $this->primaryKey(),
            'PartsLink' => $this->string(255),
            'Plastic' => $this->string(255),
            'StampedNumber' => $this->string(255),
            'Comments' => $this->text(),
            'ImageIn' => $this->string(255),
            'ImageOut' => $this->string(255),
            'ImageTop' => $this->string(255),
        ]);

        $this->createIndex('PartsLink', 'qryUSCtable', 'PartsLink');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('qryUSCtable');
    }
}
