<?php

use yii\db\Migration;

class m170607_140529_bumper_comment extends Migration
{
    public function up()
    {
        $this->createTable('bumper_comment', [
            'id' => $this->primaryKey(),
            'parts_link' => $this->string()->notNull(),
            'text' => $this->text(),
        ]);

        $this->createIndex('partsLink', 'bumper_comment', 'parts_link');
    }

    public function down()
    {
        $this->dropTable('bumper_comment');
    }
}