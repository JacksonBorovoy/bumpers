<?php

use yii\db\Migration;

class m170524_065819_add_youtube_link_to_post extends Migration
{
    public function up()
    {
        $this->addColumn('post', 'video_url', $this->string(1024));

    }

    public function down()
    {
        $this->dropColumn('post', 'video_url');
    }
}
