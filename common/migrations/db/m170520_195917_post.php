<?php

use yii\db\Migration;

class m170520_195917_post extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post_category}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title' => $this->string(512)->notNull(),
            'body' => $this->text(),
            'parent_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%post}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title' => $this->string(512)->notNull(),
            'body' => $this->text()->notNull(),
            'category_id' => $this->integer(),
            'year' => $this->integer(),
            'make' => $this->string(),
            'model' => $this->string(),
            'thumbnail_base_url' => $this->string(1024),
            'thumbnail_path' => $this->string(1024),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'published_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%post_attachment}}', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'path' => $this->string()->notNull(),
            'base_url' => $this->string(),
            'type' => $this->string(),
            'size' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'order' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%tag}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'frequency' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createTable('{{%post_tag_assn}}', [
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('', '{{%post_tag_assn}}', ['post_id', 'tag_id']);

        $this->addForeignKey('fk_post_attachment_post', '{{%post_attachment}}', 'post_id', '{{%post}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_post_author', '{{%post}}', 'created_by', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_post_updater', '{{%post}}', 'updated_by', '{{%user}}', 'id', 'set null', 'cascade');
        $this->addForeignKey('fk_post_category', '{{%post}}', 'category_id', '{{%post_category}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_post_category_section', '{{%post_category}}', 'parent_id', '{{%post_category}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_post_attachment_post', '{{%post_attachment}}');
        $this->dropForeignKey('fk_post_author', '{{%post}}');
        $this->dropForeignKey('fk_post_updater', '{{%post}}');
        $this->dropForeignKey('fk_post_category', '{{%post}}');
        $this->dropForeignKey('fk_post_category_section', '{{%post_category}}');

        $this->dropTable('{{%post_attachment}}');
        $this->dropTable('{{%post}}');
        $this->dropTable('{{%post_category}}');
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%post_tag_assn}}');
    }
}
