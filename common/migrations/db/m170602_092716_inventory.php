<?php

use yii\db\Migration;

class m170602_092716_inventory extends Migration
{
    public function up()
    {
        $this->createTable('inventory', [
            'id' => $this->primaryKey(),
            'parts_link' => $this->string()->notNull(),
            'grade' => $this->string(5)->notNull()->defaultValue('A'),
            'is_finished' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'created_at' => $this->dateTime(),
            'sold_at' => $this->dateTime(),
            'discarded_at' => $this->dateTime(),
        ]);

        $this->createIndex('partsLink', 'inventory', 'parts_link');

        $this->createTable('lost_sales', [
            'id' => $this->primaryKey(),
            'parts_link' => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex('partsLink', 'lost_sales', 'parts_link');
    }

    public function down()
    {
        $this->dropTable('inventory');
        $this->dropTable('lost_sales');
    }
}
