<?php

namespace common\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Lightbox extends \branchonline\lightbox\Lightbox
{
    public function run()
    {
        $html = '';
        foreach ($this->files as $file) {
            if (!isset($file['thumb']) || !isset($file['original'])) {
                continue;
            }

            $attributes = [
                'data-title' => isset($file['title']) ? $file['title'] : '',
            ];

            if (isset($file['group'])) {
                $attributes['data-lightbox'] = $file['group'];
            } else {
                $attributes['data-lightbox'] = 'image-' . uniqid();
            }

            $thumbOptions = isset($file['thumbOptions']) ? $file['thumbOptions'] : [];
            $linkOptions = isset($file['linkOptions']) ? $file['linkOptions'] : [];
            $withWrapper = isset($file['withWrapper']) ? $file['withWrapper'] : false;
            $wrapperClass = isset($file['wrapperClass']) ? $file['wrapperClass'] : null;

            $img = Html::img($file['thumb'], $thumbOptions);
            $a = Html::a($img, $file['original'], ArrayHelper::merge($attributes, $linkOptions));

            if ($withWrapper) {
                $a = "<div class='$wrapperClass'>" . $a . "</div>";
            }

            $html .= $a;
        }
        return $html;
    }
}