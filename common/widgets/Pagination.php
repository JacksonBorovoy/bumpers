<?php

namespace common\widgets;

use yii\helpers\Html;
use yii\widgets\LinkPager;

class Pagination extends LinkPager
{
    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();

        // first page
        $firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
        if ($firstPageLabel !== false) {
            $buttons[] = $this->renderPageButton($firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
        }

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttons[] = $this->renderPrevPageButton($page, $currentPage <= 0);
        }

        // internal pages
        list($beginPage, $endPage) = $this->getPageRange();
        $links = [];
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $links[] = $this->renderCustomPageButton($i + 1, $i, $i == $currentPage);
        }
        $buttons[] = Html::tag('div', implode("\n", $links), ['class' => 'col-6 col-sm-8 col-md-8 col-lg-10 text-center align-self-center paginationPageList']);

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->renderNextPageButton($page, $currentPage >= $pageCount - 1);
        }

        // last page
        $lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
        if ($lastPageLabel !== false) {
            $buttons[] = $this->renderPageButton($lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        $pagination = Html::tag('div', implode("\n", $buttons), ['class' => 'row']);
        $container = Html::tag('div', $pagination, ['class' => 'container']);

        return Html::tag('div', $container, $this->options);
    }

    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param int $page the page number
     * @param bool $disabled whether this page button is disabled
     * @return string the rendering result
     */
    protected function renderPrevPageButton($page, $disabled)
    {
        $arrow = Html::tag('i', '', ['class' => 'fa fa-angle-left', 'aria-hidden' => 'true']);

        if ($disabled) {
            $a = Html::a($arrow, 'javascript:void(0)', ['class' => 'paginationArrow disabled']);

            return Html::tag('div', $a, ['class' => 'col-3 col-sm-2 col-md-2 col-lg-1']);
        }

        $linkOptions = $this->linkOptions;

        $linkOptions['data-page'] = $page;

        $linkOptions['class'] = 'paginationArrow';

        $a = Html::a($arrow, $this->pagination->createUrl($page), $linkOptions);

        return Html::tag('div', $a, ['class' => 'col-3 col-sm-2 col-md-2 col-lg-1']);
    }

    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param string $label the text label for the button
     * @param int $page the page number
     * @param bool $active whether this page button is active
     * @return string the rendering result
     */
    protected function renderCustomPageButton($label, $page, $active)
    {
        $linkOptions = $this->linkOptions;

        $linkOptions['data-page'] = $page;

        $linkOptions['class'] = 'paginationPage';

        if ($active) {
            Html::addCssClass($linkOptions, 'paginationPageActive');
        }

        return Html::a($label, $this->pagination->createUrl($page), $linkOptions);
    }

    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param int $page the page number
     * @param bool $disabled whether this page button is disabled
     * @return string the rendering result
     */
    protected function renderNextPageButton($page, $disabled)
    {
        $arrow = Html::tag('i', '', ['class' => 'fa fa-angle-right', 'aria-hidden' => 'true']);

        if ($disabled) {
            $a = Html::a($arrow, 'javascript:void(0)', ['class' => 'paginationArrow disabled']);

            return Html::tag('div', $a, ['class' => 'col-3 col-sm-2 col-md-2 col-lg-1 text-right']);
        }

        $linkOptions = $this->linkOptions;

        $linkOptions['data-page'] = $page;

        $linkOptions['class'] = 'paginationArrow';

        $a = Html::a($arrow, $this->pagination->createUrl($page), $linkOptions);

        return Html::tag('div', $a, ['class' => 'col-3 col-sm-2 col-md-2 col-lg-1 text-right']);
    }
}