<?php

namespace frontend\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;

    public $email;

    public $body;

    public $reCaptcha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email', 'body', 'reCaptcha'], 'required'],
            [['name', 'body'], 'filter', 'filter' => 'strip_tags'],
            ['email', 'email'],
            ['reCaptcha', ReCaptchaValidator::className()],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('frontend', 'Name'),
            'email' => Yii::t('frontend', 'Email'),
            'body' => Yii::t('frontend', 'Comment'),
            'reCaptcha' => Yii::t('frontend', 'reCaptcha')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            return Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom(Yii::$app->params['robotEmail'])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject('Contact Us Request')
                ->setTextBody($this->body)
                ->send();
        } else {
            return false;
        }
    }
}
