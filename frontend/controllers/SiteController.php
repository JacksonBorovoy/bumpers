<?php

namespace frontend\controllers;

use common\models\Bumper;
use common\models\ContactUsPage;
use common\models\DeliveryPage;
use common\models\HomePage;
use common\models\WarrantyPage;
use frontend\models\ContactForm;
use frontend\models\search\PostSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Home Page
     * @return string
     */
    public function actionIndex()
    {
        $homePageModel = HomePage::findOne(1);

        $contactUsPageModel = ContactUsPage::findOne(1);

        $warrantyPageModel = WarrantyPage::findOne(1);

        $deliveryPageModel = DeliveryPage::findOne(1);

        $yearList = Bumper::getYearList();

        $makeList = Bumper::getMakeList();

        $contactForm = new ContactForm();

        if ($contactForm->load(Yii::$app->request->post())) {
            if ($contactForm->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('alert', [
                    'body' => 'Thank you for contacting us. We will respond to you as soon as possible.',
                    'options' => ['class' => 'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('alert', [
                    'body' => 'There was an error sending email.',
                    'options' => ['class' => 'alert-danger']
                ]);
            }
        }

        return $this->render('index', [
            'homePage' => $homePageModel,
            'contactUsPage' => $contactUsPageModel,
            'yearList' => $yearList,
            'makeList' => $makeList,
            'warrantyPage' => $warrantyPageModel,
            'deliveryPage' => $deliveryPageModel,
            'contactForm' => $contactForm,
        ]);
    }

    /**
     * Gallery page
     * @return string
     */
    public function actionGallery()
    {
        $yearList = Bumper::getYearList();

        $makeList = Bumper::getMakeList();

        $warrantyPageModel = WarrantyPage::findOne(1);

        $deliveryPageModel = DeliveryPage::findOne(1);

        $searchModel = new PostSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('gallery', [
            'yearList' => $yearList,
            'makeList' => $makeList,
            'dataProvider' => $dataProvider,
            'warrantyPage' => $warrantyPageModel,
            'deliveryPage' => $deliveryPageModel,
        ]);
    }

    /**
     * Action to GET Model list via AJAX call
     */
    public function actionGetAjaxModelList()
    {
        $allParams = Yii::$app->request->post('depdrop_all_params', null);
        $make = ArrayHelper::getValue($allParams, 'make');
        $selectedModel = ArrayHelper::getValue($allParams, 'model-hidden');
        if (!empty($make)) {
            $list = Bumper::getModelList($make);
            $response = [];
            foreach ($list as $item) {
                $response[] = ['id' => $item, 'name' => $item];
            }

            echo Json::encode(['output' => $response, 'selected' => $selectedModel]);
            return;
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * Contact Us page
     * @return string|\yii\web\Response
     */
    public function actionContact()
    {
        $contactUsPageModel = ContactUsPage::findOne(1);

        $warrantyPageModel = WarrantyPage::findOne(1);

        $deliveryPageModel = DeliveryPage::findOne(1);

        $contactForm = new ContactForm();

        if ($contactForm->load(Yii::$app->request->post())) {
            if ($contactForm->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('alert', [
                    'body' => 'Thank you for contacting us. We will respond to you as soon as possible.',
                    'options' => ['class' => 'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('alert', [
                    'body' => 'There was an error sending email.',
                    'options' => ['class' => 'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
            'contactUsPage' => $contactUsPageModel,
            'warrantyPage' => $warrantyPageModel,
            'deliveryPage' => $deliveryPageModel,
            'contactForm' => $contactForm,
        ]);
    }
}
