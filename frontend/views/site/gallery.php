<?php
use common\widgets\Pagination;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $yearList
 * @var array $makeList
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\models\Post $item
 * @var $this \yii\web\View
 * @var $warrantyPage \common\models\WarrantyPage
 * @var $deliveryPage \common\models\DeliveryPage
 */

$this->title = 'Gallery';
$pagination = $dataProvider->pagination;
$items = $dataProvider->getModels();
?>
<section class="page gallery">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 text-center text-lg-left">
                <h1 class="headering headeringPage">Gallery</h1>
            </div>
            <div class="col-lg-4 text-center text-lg-right">
                <div class="galleryFilters">
                    <select name="" class="primarySelect" select-style>
                        <option data-display="Year">Year</option>
                        <?php foreach ($yearList as $item): ?>
                            <option value="<?= $item ?>"><?= $item ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="" class="primarySelect" select-style>
                        <option data-display="Make">Make</option>
                        <?php foreach ($makeList as $item): ?>
                            <option value="<?= $item ?>"><?= $item ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= DepDrop::widget([
                        'options' => [
                            'id' => 'model',
                            'class' => 'primarySelect',
                            'select-style' => 'select-style',
                        ],
                        'name' => 'model',
                        'pluginOptions' => [
                            'depends' => ['make'],
                            'placeholder' => 'Select model...',
                            'url' => Url::to(['get-ajax-model-list']),
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($items as $item): ?>
                <div class="col-md-6 col-lg-4">
                    <a href="" class="galleryItem">
                        <span class="galleryItemHead">
                            <span class="galleryItemImg">
                                <?= Html::img(Yii::$app->glide->createSignedUrl([
                                    'glide/index',
                                    'path' => $item->thumbnail_path,
                                    'w' => 370,
                                    'h' => 245,
                                ], true)) ?>
                            </span>
                            <span class="galleryItemInfo">
                                <span class="galleryItemTitle"><?= $item->title ?></span>
                                <ul class="galleryItemChar">
                                    <li class="galleryItemCharItem"><?= $item->year ?></li>
                                    <li class="galleryItemCharItem"><?= $item->make ?></li>
                                    <li class="galleryItemCharItem"><?= $item->model ?></li>
                                </ul>
                            </span>
                        </span>
                        <span class="galleryItemDesc" dotdot><?= $item->body ?></span>
                    </a>
                </div>
            <?php endforeach; ?>
            <div class="banner text-center">LEADER BOARD <span class="bannerSize">728 x 90 px</span></div>
        </div>
    </div>
</section>

<?= Pagination::widget([
    'pagination' => $pagination
]) ?>

<!-- Modals -->
<div class="modal fade"
     id="modalWarranty"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMid" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody">
                <?= $warrantyPage->text ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"
     id="modalDelivery"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMid" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody nopadding">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 modalColHalf">
                            <?= $deliveryPage->text ?>
                        </div>
                        <div class="col-md-8 modalColImg">
                            <?= Html::img('@web/images/delivery.jpg', ['width' => 980, 'height' => 1183]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>