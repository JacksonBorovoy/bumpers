<?php
use common\models\ContactUsPage;
use common\models\DeliveryPage;
use common\models\WarrantyPage;
use frontend\models\ContactForm;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $contactUsPage ContactUsPage
 * @var $warrantyPage WarrantyPage
 * @var $deliveryPage DeliveryPage
 * @var $this View
 * @var $contactForm ContactForm
 */

$this->title = 'Contact';
?>

<section class="page aboutUsWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (Yii::$app->session->hasFlash('alert')) {
                    echo Alert::widget(Yii::$app->session->getFlash('alert'));
                } ?>
            </div>

            <div class="col-md-6 col-lg-5 aboutUsFormSection">
                <div class="text-center text-md-left aboutUsMobileTitle">
                    <h1 class="headering headeringPage">About Us</h1>
                </div>
                <?php if (!empty($contactUsPage->about_us)): ?>
                    <p><?= $contactUsPage->about_us ?></p>
                <?php endif; ?>

                <table class="aboutUsTable">
                    <tbody>
                    <?php if (!empty($contactUsPage->address)): ?>
                        <tr>
                            <td class="aboutUsTableTitle">Address:</td>
                            <td class="aboutUsTableText"><?= $contactUsPage->address ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if (!empty($contactUsPage->phone_number)): ?>
                        <tr>
                            <td class="aboutUsTableTitle">Phone:</td>
                            <td class="aboutUsTableText"><?= $contactUsPage->phone_number ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if (!empty($contactUsPage->fax_number)): ?>
                        <tr>
                            <td class="aboutUsTableTitle">Fax:</td>
                            <td class="aboutUsTableText"><?= $contactUsPage->fax_number ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if (!empty($contactUsPage->email)): ?>
                        <tr>
                            <td class="aboutUsTableTitle">Email:</td>
                            <td class="aboutUsTableText"><?= $contactUsPage->email ?></td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>

                <?php $place = $contactUsPage->map_lat . ',' . $contactUsPage->map_lng ?>
                <iframe class="aboutUsMapMobileSection"
                        src="https://www.google.com/maps/embed/v1/place?q=<?= $place ?>&key=<?= Yii::$app->params['googleMapsApiKey'] ?>"></iframe>

                <?php $form = ActiveForm::begin(['options' => ['class' => 'form']]) ?>
                <div class="row">
                    <div class="col-md-12 aboutUsFormItem">
                        <?= $form->field($contactForm, 'name')->begin() ?>

                        <?= Html::activeTextInput($contactForm, 'name', [
                            'class' => 'formItem',
                            'form-label-replace' => 'form-label-replace',
                        ]) ?>

                        <span class="formLabel required">Your Name</span>

                        <?= Html::error($contactForm, 'name', ['class' => 'help-block']); ?>

                        <?= $form->field($contactForm, 'name')->end() ?>
                    </div>
                    <div class="col-12 aboutUsFormItem">
                        <?= $form->field($contactForm, 'email')->begin() ?>

                        <?= Html::activeInput('email', $contactForm, 'email', [
                            'class' => 'formItem',
                            'form-label-replace' => 'form-label-replace',
                        ]) ?>

                        <span class="formLabel required">Your Email</span>

                        <?= Html::error($contactForm, 'email', ['class' => 'help-block']); ?>

                        <?= $form->field($contactForm, 'email')->end() ?>
                    </div>
                    <div class="col-12 aboutUsFormItem">
                        <?= $form->field($contactForm, 'body')->begin() ?>

                        <?= Html::activeTextarea($contactForm, 'body', [
                            'class' => 'formItem formTextarea',
                            'form-label-replace' => 'form-label-replace',
                        ]) ?>

                        <span class="formLabel required">Comment</span>

                        <?= Html::error($contactForm, 'body', ['class' => 'help-block']); ?>

                        <?= $form->field($contactForm, 'body')->end() ?>
                    </div>
                    <div class="col-12 aboutUsFormItem">
                        <?= $form->field($contactForm, 'reCaptcha')->widget(ReCaptcha::className(), [
                            'widgetOptions' => [
                                'align' => 'center'
                            ],
                        ])->label(false) ?>
                    </div>
                    <div class="col-12 aboutUsFormItem text-center">
                        <?= Html::submitInput('Send', ['class' => 'primaryBtn formSubmit']) ?>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>

            <div class="col-md-6 col-lg-7">
                <iframe class="aboutUsMapSection"
                        src="https://www.google.com/maps/embed/v1/place?q=<?= $place ?>&key=<?= Yii::$app->params['googleMapsApiKey'] ?>"></iframe>
            </div>
        </div>
    </div>
</section>
<!-- Modals -->
<div class="modal fade"
     id="modalWarranty"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMid" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody">
                <?= $warrantyPage->text ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"
     id="modalDelivery"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMid" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody nopadding">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 modalColHalf">
                            <?= $deliveryPage->text ?>
                        </div>
                        <div class="col-md-8 modalColImg">
                            <?= Html::img('@web/images/delivery.jpg', ['width' => 980, 'height' => 1183]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
