<?php

use common\models\Bumper;
use common\models\ContactUsPage;
use common\models\HomePage;
use frontend\models\ContactForm;
use himiklab\yii2\recaptcha\ReCaptcha;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var array $yearList */
/* @var array $makeList */
/* @var $homePage HomePage */
/* @var $contactUsPage ContactUsPage */
/* @var $warrantyPage \common\models\WarrantyPage */
/* @var $deliveryPage \common\models\DeliveryPage */
/* @var $contactForm ContactForm */

$this->title = Yii::$app->name;
?>
<section class="mainScreen mainScreenVideo">
    <div class="mainScreenVideoWrap">
        <?php if ($homePage->videobackground_path): ?>
            <?php $videoUrl = $homePage->videobackground_base_url . '/' . $homePage->videobackground_path ?>
            <video src="<?= $videoUrl ?>" autoplay loop></video>
        <?php endif; ?>
    </div>
    <div class="container-fluid">
        <div class="mainScreenContent">
            <div class="mainScreenLogo"><a href="<?= Url::home() ?>" class="mainScreenLogoText">Logotype</a></div>
            <h1 class="headering"><?= $homePage->video_title ?></h1>
            <p><?= $homePage->video_text ?></p>

            <?php if (getYouTubeVideoId($homePage->video_link)): ?>
                <button class="btn watchVideoBtn" data-toggle="modal" data-target="#modalVideo" watch-video>Watch the
                    Video
                </button>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php if ($homePage->imgbefore_path && $homePage->imgafter_path): ?>
    <section class="lasestWork">
        <div class="container">
            <div class="lasestWorkWrap">
                <div class="lasestWorkContent">
                    <h2 class="headering">Latest Work</h2>
                    <p>High-Quality, Remanufactured Bumper Covers</p>
                </div>
                <div latest-images class="twentytwenty-container lasestWorkImages">
                    <?= Html::img(Yii::$app->glide->createSignedUrl([
                        'glide/index',
                        'path' => $homePage->imgbefore_path,
                        'w' => 1170,
                        'h' => 406,
                    ], true)) ?>
                    <?= Html::img(Yii::$app->glide->createSignedUrl([
                        'glide/index',
                        'path' => $homePage->imgafter_path,
                        'w' => 1170,
                        'h' => 406,
                    ], true)) ?>
                </div>
                <a href="#" class="btn primaryBtn lasestWorkBtn">Gallery</a>
            </div>
        </div>
    </section>
<?php endif; ?>

<section class="services">
    <div class="container">
        <h2 class="headering"><?= $homePage->services_title ?></h2>
        <p><?= $homePage->services_text ?></p>
        <div class="servicesToggles">
            <div class="servicesTogglesItem servicesTogglesActive" services-toggle="recondition">
                <div class="servicesTogglesMiddle">
                    <div class="servicesIcon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="67" height="67" viewBox="0 0 67 67">
                            <path fill="#444"
                                  fill-rule="nonzero"
                                  d="M65.849 57.899L51.152 43.257l.93-2.473c.174-.462.061-.982-.289-1.33l-2.192-2.185a1.265 1.265 0 0 0-1.785 0l-2.755 2.745-5.76-5.738 12.771-12.721a11.23 11.23 0 0 1 4.487-2.708 9.765 9.765 0 0 0 3.891-2.37 9.601 9.601 0 0 0 2.85-7.013 9.613 9.613 0 0 0-3.077-6.924 1.265 1.265 0 0 0-2.067.542l-1.6 5.11a2.46 2.46 0 0 1-1.218 1.455 2.479 2.479 0 0 1-1.895.171 2.467 2.467 0 0 1-1.633-3.101l1.582-5.051c.13-.416.035-.87-.252-1.198A1.263 1.263 0 0 0 51.985.05a9.667 9.667 0 0 0-4.838 2.27 9.703 9.703 0 0 0-3.258 8.375c.361 3.232-.795 6.444-3.173 8.813l-8.119 8.088-17.53-17.464-.317-3.425a1.256 1.256 0 0 0-.586-.95L5.254.193a1.266 1.266 0 0 0-1.563.176L.37 3.678a1.253 1.253 0 0 0-.177 1.556L5.78 14.11c.209.332.561.548.953.584l3.438.316 17.53 17.464-12.77 12.72a11.236 11.236 0 0 1-4.49 2.708 9.637 9.637 0 0 0-3.89 2.372 9.601 9.601 0 0 0-2.85 7.012 9.613 9.613 0 0 0 3.076 6.923 1.266 1.266 0 0 0 2.068-.542l1.602-5.109a2.462 2.462 0 0 1 1.218-1.456 2.48 2.48 0 0 1 1.895-.17 2.478 2.478 0 0 1 1.632 3.102l-1.58 5.05c-.131.415-.036.868.25 1.197a1.263 1.263 0 0 0 1.156.417 9.694 9.694 0 0 0 4.838-2.268 9.717 9.717 0 0 0 3.258-8.378c-.363-3.23.794-6.441 3.173-8.812l8.118-8.086 5.76 5.737-2.756 2.745a1.254 1.254 0 0 0 0 1.777l2.193 2.185c.35.349.873.461 1.335.288l2.483-.927L58.118 65.6a3.933 3.933 0 0 0 2.785 1.147 3.933 3.933 0 0 0 2.784-1.147l2.162-2.154a3.917 3.917 0 0 0 0-5.547zM42.5 21.286c2.923-2.912 4.343-6.874 3.896-10.87a7.194 7.194 0 0 1 3.864-7.19l-.858 2.741a4.97 4.97 0 0 0 3.29 6.25c2.636.819 5.45-.651 6.273-3.277l.89-2.847a7.12 7.12 0 0 1 .922 3.41 7.113 7.113 0 0 1-2.11 5.195 7.13 7.13 0 0 1-2.882 1.756c-2.089.67-3.99 1.82-5.497 3.323l-12.77 12.72-3.137-3.124 8.119-8.087zM24.503 45.463c-2.925 2.914-4.345 6.875-3.896 10.868a7.205 7.205 0 0 1-2.416 6.211 7.098 7.098 0 0 1-1.447.98l.858-2.74c.814-2.604-.629-5.382-3.221-6.227a4.996 4.996 0 0 0-3.887.322 4.96 4.96 0 0 0-2.455 2.933l-.892 2.846a7.117 7.117 0 0 1-.923-3.41 7.113 7.113 0 0 1 2.112-5.195 7.134 7.134 0 0 1 2.88-1.757c2.09-.67 3.992-1.819 5.5-3.322l12.77-12.72 3.136 3.124-8.119 8.087zM11.507 12.786a1.26 1.26 0 0 0-1.31-.298l-2.61-.24-4.722-7.501 1.9-1.893 7.53 4.704.24 2.601a1.254 1.254 0 0 0 .3 1.304l30.442 30.328-1.328 1.323-30.442-30.328zm31.533 35.63l-2.227.832-.727-.724 8.622-8.589.727.724-.835 2.218-5.56 5.54zm21.024 13.252l-2.162 2.154a1.42 1.42 0 0 1-2 0l-14.38-14.325 4.163-4.146 14.38 14.325c.55.55.55 1.443 0 1.992z"/>
                        </svg>
                    </div>
                    <div class="servicesTitle">Bumpers reconditions</div>
                    <p>Encore Bumpers reconditions OEM bumper covers</p>
                    <a href="#" class="simpleLink servicesLink">Go to page <i class="fa fa-angle-right"
                                                                              aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="servicesTogglesItem" services-toggle="inventory">
                <div class="servicesTogglesMiddle">
                    <div class="servicesIcon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="68" viewBox="0 0 53 68">
                            <g fill="#444" fill-rule="evenodd">
                                <path d="M49.936 64.707H2.994V9.842h6.858a7.407 7.407 0 0 0 7.248 5.882h18.73a7.405 7.405 0 0 0 7.245-5.882h6.86v54.865zM12.693 3.128h27.541V8.35c0 2.42-1.976 4.39-4.404 4.39H17.1c-2.43 0-4.407-1.969-4.407-4.39V3.128zm38.74 3.73h-8.205V1.636c0-.824-.67-1.492-1.497-1.492H11.197c-.827 0-1.498.668-1.498 1.492v5.222H1.497C.67 6.858 0 7.526 0 8.35V66.2c0 .824.67 1.492 1.497 1.492h49.936c.827 0 1.497-.668 1.497-1.492V8.35c0-.824-.67-1.492-1.497-1.492z"/>
                                <path d="M9.998 25.52h5.588v-5.569H9.998v5.57zm7.085-8.553H8.501c-.827 0-1.497.668-1.497 1.492v8.553c0 .824.67 1.492 1.497 1.492h8.582c.827 0 1.497-.668 1.497-1.492V18.46c0-.824-.67-1.492-1.497-1.492zM21.574 27.012c0 .824.67 1.492 1.497 1.492h20.358c.827 0 1.497-.668 1.497-1.492 0-.823-.67-1.491-1.497-1.491H23.07c-.827 0-1.497.668-1.497 1.491M9.998 40.24h5.588v-5.57H9.998v5.57zm7.085-8.553H8.501c-.827 0-1.497.668-1.497 1.492v8.553c0 .824.67 1.492 1.497 1.492h8.582c.827 0 1.497-.668 1.497-1.492V33.18c0-.824-.67-1.492-1.497-1.492zM43.429 40.24H23.07c-.827 0-1.497.668-1.497 1.492s.67 1.492 1.497 1.492h20.358c.827 0 1.497-.668 1.497-1.492s-.67-1.492-1.497-1.492M9.998 54.96h5.588v-5.57H9.998v5.57zm7.085-8.553H8.501c-.827 0-1.497.668-1.497 1.492v8.553c0 .824.67 1.492 1.497 1.492h8.582c.827 0 1.497-.668 1.497-1.492v-8.553c0-.824-.67-1.492-1.497-1.492zM43.429 54.96H23.07c-.827 0-1.497.668-1.497 1.492s.67 1.492 1.497 1.492h20.358c.827 0 1.497-.668 1.497-1.492s-.67-1.492-1.497-1.492"/>
                            </g>
                        </svg>
                    </div>
                    <div class="servicesTitle">Bumper Inventory</div>
                    <p><?= Yii::$app->formatter->asDecimal(Bumper::getTotalNumber()) ?> Bumper Covers IN STOCK</p>
                    <a href="" class="servicesLink">Go to page <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="servicesForms">
        <div class="servicesFormsItem servicesFormsActive" services-form>
            <div class="container">
                <form action="" class="form">
                    <div class="row">
                        <div class="col-lg-6 servicesPadding">
                            <div class="row">
                                <div class="col-md-6 formCol">
                                    <input class="formItem" type="text" required form-label-replace/>
                                    <span class="formLabel required">Your Name</span>
                                </div>
                                <div class="col-md-6 formCol">
                                    <input class="formItem hasError" value="Email.com" type="email"
                                           form-label-replace/>
                                    <span class="formLabel required">Your Email</span>
                                </div>
                                <div class="col-md-6 formCol">
                                    <input class="formItem" type="text" form-label-replace/>
                                    <span class="formLabel">+1 (__) ___-___</span>
                                </div>
                                <div class="col-md-6 formCol">
                                    <input class="formItem" type="text" form-label-replace/>
                                    <span class="formLabel">Address</span>
                                </div>
                                <div class="col-md-6 formCol">
                                    <select name="" class="formItem formSelect" select-style>
                                        <option data-display="City">City</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-6 formCol">
                                    <select name="" class="formItem formSelect" select-style>
                                        <option data-display="Country">Country</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 servicesPadding">
                            <div class="row">
                                <div class="col-md-6 formCol">
                                    <select name="" class="formItem formSelect" select-style>
                                        <option data-display="State / Provinve">State / Provinve</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-md-6 formCol">
                                    <input class="formItem" type="text" form-label-replace/>
                                    <span class="formLabel">Zip / Postal Code</span>
                                </div>
                                <div class="col-md-6 formCol">
                                    <select name=""
                                            class="formItem formSelect required hasError"
                                            select-style
                                            form-select-required>
                                        <option data-display="Front / Rear">Front / Rear</option>
                                        <option value="Front">Front</option>
                                        <option value="Rear">Rear</option>
                                    </select>
                                </div>
                                <div class="col-md-6 formCol">
                                    <select name=""
                                            class="formItem formSelect required"
                                            select-style
                                            form-select-required>
                                        <option data-display="Year">Year</option>
                                        <?php foreach ($yearList as $item): ?>
                                            <option value="<?= $item ?>"><?= $item ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6 formCol">
                                    <select name=""
                                            class="formItem formSelect required"
                                            select-style
                                            form-select-required>
                                        <option data-display="Make">Make</option>
                                        <?php foreach ($makeList as $item): ?>
                                            <option value="<?= $item ?>"><?= $item ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6 formCol">
                                    <?= DepDrop::widget([
                                        'options' => [
                                            'id' => 'model',
                                            'class' => 'formItem formSelect',
                                            'select-style' => 'select-style',
                                        ],
                                        'name' => 'model',
                                        'pluginOptions' => [
                                            'depends' => ['make'],
                                            'placeholder' => 'Select model...',
                                            'url' => Url::to(['get-ajax-model-list']),
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 formCol">
                            <div id="drag-and-drop" class="dropzone formUpload">
                                <p class="formUploadCount">Load Photo (<span drag-and-drop-count>0</span>
                                    <span class="colorGrayDark">to 10</span>)</p>
                                <div class="formUploadPreviews" drag-and-drop-previews>
                                    <div class="dz-default dz-message formUploadMsg">
                                        <div class="formUploadMsgAlign">
                                            <div class="formUploadMsgIcon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="18"
                                                     height="25"
                                                     viewBox="0 0 18 25">
                                                    <g fill="#E4E4E4" fill-rule="evenodd">
                                                        <path d="M2.857 12.054l.925-.831 5.065 5.399 5.119-5.456.925.831-6.044 6.443zM0 24.782h17.732v-1.229H0z"/>
                                                        <path d="M8.246 17.296h1.256V.41H8.246zM0 24.782h1.256v-6.888H0zM16.477 24.782h1.256v-6.888h-1.256z"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <b>Choose a file</b><br/>
                                            <span class="formUploadMsgDroptext">or Drag and drop</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="fallback"><input type="file" name="file" multiple/></div>
                            </div>
                        </div>
                        <div class="col-lg-6 formCol">
                            <div class="row">
                                <div class="col-12 formCol formColTop">
                                            <textarea class="formItem formTextarea"
                                                      name=""
                                                      id=""
                                                      cols="30"
                                                      rows="10" form-label-replace></textarea>
                                    <span class="formLabel">Comment</span>
                                </div>
                                <div class="col-md-4 col-lg-6 formCol"></div>
                                <div class="col-md-4 col-lg-6 formCol">
                                    <input value="Send" type="submit" class="primaryBtn formSubmit">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="servicesFormsItem servicesHalfBg" services-form>
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <h2 class="headering text-center text-lg-left">Find Your Model</h2>
                        <form action="" class="form">
                            <div class="row">
                                <div class="col-md-6 align-self-end formCol">
                                    <select name="year" class="formItem formSelect" select-style>
                                        <option data-display="Select Year">Select Year</option>
                                        <?php foreach ($yearList as $item): ?>
                                            <option value="<?= $item ?>"><?= $item ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6 align-self-end formCol">
                                    <select name="make" class="formItem formSelect" id="make" select-style>
                                        <option data-display="Select Make">Select Make</option>
                                        <?php foreach ($makeList as $item): ?>
                                            <option value="<?= $item ?>"><?= $item ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6 align-self-end formCol">
                                    <?= DepDrop::widget([
                                        'options' => [
                                            'id' => 'model',
                                            'class' => 'formItem formSelect',
                                            'select-style' => 'select-style',
                                        ],
                                        'name' => 'model',
                                        'pluginOptions' => [
                                            'depends' => ['make'],
                                            'placeholder' => 'Select model...',
                                            'url' => Url::to(['get-ajax-model-list']),
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-md-6 align-self-end formCol">
                                    <select name="frontRear" class="formItem formSelect" select-style>
                                        <option data-display="Select Front / Rear">Select Front / Rear</option>
                                        <option value="Front">Front</option>
                                        <option value="Rear">Rear</option>
                                    </select>
                                </div>
                                <div class="col-12 text-md-center text-lg-right formCol">
                                    <input value="Search" type="submit" class="primaryBtn formSubmit">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 servicesFormsItemBg">
                        <p class="text-center text-lg-left">Or Enter OEM Number <br class="hidden-md-down"/>/ Parts
                            Link Number</p>
                        <form action="" class="form">
                            <div class="row">
                                <div class="col col-12">
                                    <div class="text-center text-lg-left formNote"><span class="colorRed">*</span>
                                        No
                                        spaces or dashes. Other
                                        criteria will be ignored
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-9 formCol">
                                    <input class="formItem" type="text" form-label-replace/>
                                    <span class="formLabel">OEM Number</span>
                                </div>
                                <div class="col-md-6 col-lg-9 formCol">
                                    <input class="formItem" type="text" form-label-replace/>
                                    <span class="formLabel">Parts Link Number</span>
                                </div>
                                <div class="col-12 text-md-center text-lg-left formCol">
                                    <input value="Search" type="submit" disabled="true"
                                           class="primaryBtn formSubmit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="brands" services-brands>
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/ford.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/audi.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/mercedes.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/hyundai.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/cadillac.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/toyota.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/ford.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/audi.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/mercedes.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/hyundai.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/cadillac.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/toyota.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/hyundai.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/cadillac.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/toyota.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/ford.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/audi.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/mercedes.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/hyundai.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/cadillac.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/toyota.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/ford.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/audi.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-1">
                        <a href="#" class="brandsItem">
                            <img src="images/make/mercedes.jpg" width="68" height="50" alt="">
                        </a>
                    </div>
                </div>
                <button class="transparentBtn">Load More <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</section>
<section class="contactUs">
    <div class="contactUsContainer">
        <div class="row justify-content-lg-between align-items-lg-start contactUsMainRow">
            <div class="col-md-6 contactUsContent">
                <h2 class="headering">Contact Us</h2>
                <?php if ($contactUsPage->address): ?>
                    <div class="row contactUsRow">
                        <div class="col-md-3 col-lg-2 contactUsCol"><span class="contactUsTitle">Address:</span></div>
                        <div class="col-md-9 col-lg-10 contactUsCol">
                            <a href="#modalContact" data-toggle="modal">
                                    <span class="contactUsIcon"><svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="15"
                                                                     height="15"
                                                                     viewBox="0 0 15 15">
                                        <path fill="#FFB500"
                                              fill-rule="nonzero"
                                              d="M7.488 1C5.014 1 3 3.14 3 5.772c0 .686.132 1.342.393 1.948 1.122 2.61 3.273 5.367 3.906 6.15a.244.244 0 0 0 .19.092.244.244 0 0 0 .189-.092c.632-.783 2.784-3.54 3.906-6.15.26-.606.393-1.262.393-1.948C11.977 3.141 9.963 1 7.488 1zm.169 7.293a2.503 2.503 0 0 1-2.5-2.5c0-1.378 1.121-2.5 2.5-2.5 1.378 0 2.5 1.122 2.5 2.5 0 1.379-1.122 2.5-2.5 2.5z"/>
                                        </svg></span><?= $contactUsPage->address ?>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($contactUsPage->phone_number): ?>
                    <div class="row contactUsRow">
                        <div class="col-md-3 col-lg-2 contactUsCol"><span class="contactUsTitle">Phone:</span></div>
                        <div class="col-md-9 col-lg-10 contactUsCol"><?= $contactUsPage->phone_number ?></div>
                    </div>
                <?php endif; ?>
                <?php if ($contactUsPage->fax_number): ?>
                    <div class="row contactUsRow">
                        <div class="col-md-3 col-lg-2 contactUsCol"><span class="contactUsTitle">Fax:</span></div>
                        <div class="col-md-9 col-lg-10 contactUsCol"><?= $contactUsPage->fax_number ?></div>
                    </div>
                <?php endif; ?>
                <?php if ($contactUsPage->email): ?>
                    <div class="row contactUsRow">
                        <div class="col-md-3 col-lg-2 contactUsCol"><span class="contactUsTitle">Email:</span></div>
                        <div class="col-md-9 col-lg-10 contactUsCol"><a
                                    href="mailto:info@encorebumpers.com"><?= $contactUsPage->email ?></a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-6 col-lg-5 contactUsForm">
                <?php $form = ActiveForm::begin(['options' => ['class' => 'form']]) ?>
                <div class="formWrap">
                    <div class="row">
                        <div class="col-12 formCol">
                            <?= $form->field($contactForm, 'name')->begin() ?>

                            <?= Html::activeTextInput($contactForm, 'name', [
                                'class' => 'formItem',
                                'form-label-replace' => 'form-label-replace',
                            ]) ?>

                            <span class="formLabel required">Your Name</span>

                            <?= Html::error($contactForm, 'name', ['class' => 'help-block']); ?>

                            <?= $form->field($contactForm, 'name')->end() ?>
                        </div>
                        <div class="col-12 formCol">
                            <?= $form->field($contactForm, 'email')->begin() ?>

                            <?= Html::activeInput('email', $contactForm, 'email', [
                                'class' => 'formItem',
                                'form-label-replace' => 'form-label-replace',
                            ]) ?>

                            <span class="formLabel required">Your Email</span>

                            <?= Html::error($contactForm, 'email', ['class' => 'help-block']); ?>

                            <?= $form->field($contactForm, 'email')->end() ?>
                        </div>
                        <div class="col-12 formCol">
                            <?= $form->field($contactForm, 'body')->begin() ?>

                            <?= Html::activeTextarea($contactForm, 'body', [
                                'class' => 'formItem formTextarea',
                                'form-label-replace' => 'form-label-replace',
                            ]) ?>

                            <span class="formLabel required">Comment</span>

                            <?= Html::error($contactForm, 'body', ['class' => 'help-block']); ?>

                            <?= $form->field($contactForm, 'body')->end() ?>
                        </div>
                        <div class="col-12 text-center formCol">
                            <?= $form->field($contactForm, 'reCaptcha')->widget(ReCaptcha::className(), [
                                'widgetOptions' => [
                                    'align' => 'center'
                                ],
                            ])->label(false) ?>
                        </div>
                        <div class="col-12 text-center formCol">
                            <?= Html::submitInput('Send', ['class' => 'primaryBtn formSubmit']) ?>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</section>

<!-- Modals -->

<?php if (getYouTubeVideoId($homePage->video_link)): ?>
    <div class="modal fade"
         id="modalVideo"
         tabindex="-1"
         role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modalSizeMax" role="document">
            <div class="modal-content modalContent">
                <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                        <g fill="#FFF" fill-rule="evenodd">
                            <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                            <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                        </g>
                    </svg>
                </button>
                <div class="moda-body modalBody nopadding">
                    <div class="modalVideo">
                        <iframe width="560"
                                height="315"
                                src="https://www.youtube.com/embed/<?= getYouTubeVideoId($homePage->video_link) ?>"
                                frameborder="0"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="modal fade"
     id="modalContact"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMax" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody nopadding">
                <div class="modalMap">
                    <?php $place = $contactUsPage->map_lat . ',' . $contactUsPage->map_lng ?>
                    <iframe width="1100"
                            height="690"
                            frameborder="0"
                            style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?q=<?= $place ?>&key=<?= Yii::$app->params['googleMapsApiKey'] ?>"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"
     id="modalWarranty"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMid" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody">
                <?= $warrantyPage->text ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"
     id="modalDelivery"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMid" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody nopadding">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 modalColHalf">
                            <?= $deliveryPage->text ?>
                        </div>
                        <div class="col-md-8 modalColImg">
                            <?= Html::img('@web/images/delivery.jpg', ['width' => 980, 'height' => 1183]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"
     id="modalSend"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSizeMin" role="document">
        <div class="modal-content modalContent">
            <button type="button" class="modalClose" data-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                        <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                    </g>
                </svg>
            </button>
            <div class="moda-body modalBody">
                <div class="text-center">
                    <div class="modalTitle">Send an application</div>
                    <p>Nunc sit amet interdum nulla. Vestibulum rutrum nec augue non rutrum. Suspendisse interdum
                        vehicula felis maximus tempor. Vestibulum semper ex sem, rhoncus congue diam tempus
                        eget.</p>

                    <div class="modalForm">
                        <form action="" class="form">
                            <div class="row">
                                <div class="col-12 formCol">
                                    <input class="formItem" type="text" form-label-replace/>
                                    <span class="formLabel">Your Name</span>
                                </div>
                                <div class="col-12 formCol">
                                    <input class="formItem" type="email" required form-label-replace/>
                                    <span class="formLabel required ">Your Email</span>
                                </div>
                                <div class="col-12 formCol">
                                            <textarea class="formItem formTextarea"
                                                      cols="30"
                                                      rows="10" required form-label-replace></textarea>
                                    <span class="formLabel required">Message</span>
                                </div>
                                <div class="col-12 text-center formCol">
                                    <input value="Send" type="submit" class="primaryBtn formSubmit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"
     id="modalSuccess"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modalSize" role="document">
        <div class="modal-content modalContent noclose">
            <div class="moda-body modalBody nopadding">
                <div class="row text-center align-items-center modalAlign">
                    <div class="col">
                        <svg xmlns="http://www.w3.org/2000/svg" width="31" height="25" viewBox="0 0 31 25">
                            <path fill="#81AE50" fill-rule="evenodd"
                                  d="M26.43 0L10.255 16.177 3.978 9.903 0 13.881l6.274 6.276 3.975 3.982 3.987-3.98L30.41 3.978z"/>
                        </svg>
                        <p>Your order has been submitted.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>