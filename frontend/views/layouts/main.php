<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php');
$action = $this->context->action->id;

$isHomeActive = $action == 'index' ? 'menuItemActive' : '';
$isGalleryActive = $action == 'gallery' ? 'menuItemActive' : '';
$isServicesActive = $action == 'services' ? 'menuItemActive' : '';
$isContactUsActive = $action == 'contact' ? 'menuItemActive' : '';
?>

    <header class="header" menu-block>
        <div class="container">
            <button class="menuToggle" menu-toggle>
                <i class="menuToggleOpen">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="20" viewBox="0 0 24 20">
                        <g fill="#FFB500" fill-rule="evenodd">
                            <path d="M0 0h24v2.857H0zM0 8.571h24v2.857H0zM0 17.143h24V20H0z"/>
                        </g>
                    </svg>
                </i>
                <i class="menuToggleClose">
                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                        <g fill="#FFF" fill-rule="evenodd">
                            <path d="M16.546 0l1.946 1.947L1.947 18.492 0 16.546"/>
                            <path d="M0 1.947L1.947 0l16.545 16.546-1.946 1.946"/>
                        </g>
                    </svg>
                </i>
            </button>
            <div class="menuInner">
                <div class="row align-items-stretch justify-content-between">
                    <div class="col-12 col-xl-3">
                        <a href="<?= Url::home() ?>" class="headerLogo"><span class="headerLogoText">Logotype</span></a>
                    </div>
                    <div class="col-12 col-xl-6">
                        <div class="menu">
                            <ul class="menuWrap">
                                <li class="menuItem <?= $isHomeActive ?>">
                                    <a href="<?= Url::home() ?>" class="menuLink">Home</a>
                                </li>
                                <li class="menuItem <?= $isGalleryActive ?>">
                                    <a href="<?= Url::to(['gallery']) ?>" class="menuLink">Gallery</a>
                                </li>
                                <li class="menuItem menuHasSub <?= $isServicesActive ?>">
                                    <a href="javascript:void(0)" class="menuLink" menu-sub>Services</a>
                                    <ul class="menuSub">
                                        <li class="menuSubItem">
                                            <a href="recondition.html" class="menuSubLink">Bumper Reconditions</a>
                                        </li>
                                        <li class="menuSubItem">
                                            <a href="inventory.html" class="menuSubLink">Bumper Inventory</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menuItem <?= $isContactUsActive ?>">
                                    <a href="<?= Url::to(['contact']) ?>" class="menuLink">Contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-xl-3">
                        <div class="menu menuModal">
                            <ul class="menuWrap">
                                <li class="menuItem">
                                    <a href="#modalDelivery" data-toggle="modal" class="menuLink">
                                        <i class="fa fa-info-circle menuIcon" aria-hidden="true"></i> Delivery
                                    </a>
                                </li>
                                <li class="menuItem">
                                    <a href="#modalWarranty" data-toggle="modal" class="menuLink">Warranty</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?= $content ?>
    <footer class="footer">
        <div class="container text-center">&copy; 2007 - 2017 Encore Bumpers - All rights reserved</div>
    </footer>

<?php $this->endContent() ?>